<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 19.03.2019
 * Time: 23:41
 */

namespace Factory;

use Factory\OperationTable,
    Factory\EquipmentTable,
    Factory\ProcessTable,
    Factory\ProductionTable,
    Factory\BranchTable,
    Factory\WorkshopTable;

class Factory
{
    public function __construct($idObject = false, $type = 'object')
    {
        if ($type == 'object') {
            $this->idObject = (int)$idObject;
            $this->nameObject = self::getNameObject($idObject);
            $this->treeObject = self::getTreeObject($idObject);
            $this->quantityObject = (int)self::getQuantityObject($idObject);
            $this->techProcesObject = self::getTechProcess($idObject);
            $this->operationsObject = self::getOperations($this->techProcesObject['ID']);
        }
        if ($type == 'structural_object') {
            $this->workshops = self::getWorkshops($idObject);
            if (!$idObject) {
                $idBranch = false;
            } else {
                $idBranch = array_unique(array_column($this->workshops, 'UF_BRANCH'));
            }
            $this->branch = self::getListBranch($idBranch);
        }
    }

    public function isFinalObject($idObject)
    {
        $idParent = ProductionTable::getList([
            'select' => ['UF_PARENT_ID_PRODUCT'],
            'filter' => ['ID' => $idObject]
        ])->Fetch();
        if ($idParent['UF_PARENT_ID_PRODUCT'] == null) {
            return true;
        }
        return false;
    }

    static public function getEquipment($idObject = false)
    {
        $filter = [];
        if ($idObject) {
            $filter = ['ID' => $idObject];
        }
        $equipments = EquipmentTable::getList([
            'select' => ['*'],
            'filter' => $filter
        ])->fetchAll();

        return $equipments;
    }

    static public function getWorkshops($idObject = false)
    {
        $filter = [];
        if ($idObject) {
            $filter = ['ID' => $idObject];
        }
        $workshops = WorkshopTable::getList([
            'select' => ['*'],
            'filter' => $filter
        ])->fetchAll();
        foreach ($workshops as $key => $workshop) {
            preg_match_all('#(?<=["])\w+(?=["])#', $workshop['UF_EQUIPMENT'], $workshops[$key]['UF_EQUIPMENT']);
            $workshops[$key]['UF_EQUIPMENT'] = $workshops[$key]['UF_EQUIPMENT'][0];
        }
        return $workshops;
    }

    static public function getListBranch($idObject = false)
    {
        $filter = [];
        if ($idObject) {
            $filter = ['ID' => $idObject];
        }
        return BranchTable::getList([
            'select' => ['*'],
            'filter' => $filter
        ])->fetchAll();
    }

    static public function getTechProcess($idObject)
    {
        return ProcessTable::getList([
            'select' => ['*'],
            'filter' => [
                'UF_PRODUCTION' => $idObject
            ]
        ])->fetchAll()[0];
    }

    static public function getNameObject($idObject)
    {
        return ProductionTable::getList([
            'select' => ['UF_NAME'],
            'filter' => ['ID' => $idObject]
        ])->fetchAll()[0]['UF_NAME'];
    }

    static public function getQuantityObject($idObject)
    {
        return ProductionTable::getList([
            'select' => ['UF_COUNT'],
            'filter' => ['ID' => $idObject]
        ])->fetchAll()[0]['UF_COUNT'];
    }

    static public function getOperations($idTechProces)
    {
        return OperationTable::getList([
            'select' => ['*'],
            'filter' => [
                'UF_TECH_PROC' => $idTechProces
            ]
        ])->fetchAll();
    }

    static public function getTreeObject($idObject = false, &$treeObject = [])
    {
        $filter = [];
        if ($idObject) {
            array_push($filter, ['UF_PARENT_ID_PRODUCT' => $idObject]);
        }
        $childObjects = ProductionTable::getList([
            'select' => ['ID', 'UF_NAME'],
            'filter' => $filter
        ])->fetchAll();

        $treeObject[$idObject]['NAME'] = ProductionTable::getList([
            'select' => ['UF_NAME'],
            'filter' => ['ID' => $idObject]
        ])->fetchAll()[0]['UF_NAME'];

        if (count($childObjects) > 0) {
            $treeObject[$idObject]['HAVE_CHILD'] = true;

            foreach ($childObjects as $childObject) {


//                $treeObject[$idObject]['QUANTITY'] = (int)self::getQuantityObject($idObject);
//                $treeObject[$idObject]['TECH_PROCESS']['OPERATIONS'] = self::getOperations($idObject);
                $treeObject[$idObject][$childObject['ID']]['NAME'] = $childObject['UF_NAME'];


                if ($idTp = self::getTechProcess($idObject)['ID'] ?? false) {
                    $treeObject[$idObject]['QUANTITY'] = (int)self::getQuantityObject($idObject);
                    $treeObject[$idObject]['TECH_PROCESS']['OPERATIONS'] = self::getOperations($idTp);
                    $treeObject[$idObject]['TECH_PROCESS']['DESCRIPTION'] = $idTp;
                }

                if ($idTp = self::getTechProcess($childObject['ID'])['ID'] ?? false) {
                    $treeObject[$idObject][$childObject['ID']]['TECH_PROCESS']['OPERATIONS'] = self::getOperations($idTp);
                    $treeObject[$idObject][$childObject['ID']]['TECH_PROCESS']['DESCRIPTION'] = $idTp;
                    $treeObject[$idObject][$childObject['ID']]['QUANTITY'] = (int)self::getQuantityObject($childObject['ID']);
                }
                self::getTreeObject($childObject['ID'], $treeObject[$idObject]);

            }

        } else {
            $treeObject[$idObject]['HAVE_CHILD'] = false;
        }

        return $treeObject;
    }
}