<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 16.03.2019
 * Time: 21:08
 */
namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ProcessTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_NAME string optional
 * <li> UF_PRODUCTION int optional
 * </ul>
 *
 * @package Bitrix\Tech
 **/

class ProcessTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_tech_process';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('PROCESS_ENTITY_ID_FIELD'),
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('PROCESS_ENTITY_UF_NAME_FIELD'),
            ),
            'UF_PRODUCTION' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PROCESS_ENTITY_UF_PRODUCTION_FIELD'),
            ),
        );
    }
}