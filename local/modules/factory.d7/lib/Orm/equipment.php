<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 16.03.2019
 * Time: 21:08
 */
namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class EquipmentTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_DESCRIPTION string optional
 * <li> UF_NAME string optional
 * </ul>
 *
 * @package Bitrix\Equipment
 **/

class EquipmentTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_equipment';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_ID_FIELD'),
            ),
            'UF_DESCRIPTION' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_UF_DESCRIPTION_FIELD'),
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_UF_NAME_FIELD'),
            ),
            'UF_COUNT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_UF_COUNT_FIELD'),
            ),
            'UF_COORDINATE' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('STOCK_ENTITY_UF_COORDINATE_FIELD'),
            )
        );
    }
}