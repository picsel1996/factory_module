<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 16.03.2019
 * Time: 21:08
 */
namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ProductionTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_NAME string optional
 * <li> UF_TYPE_PRODUCTION int optional
 * </ul>
 *
 * @package Bitrix\Production
 **/

class ProductionTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_production';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('PRODUCTION_ENTITY_ID_FIELD'),
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('PRODUCTION_ENTITY_UF_NAME_FIELD'),
            ),
            'UF_TYPE_PRODUCTION' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PRODUCTION_ENTITY_UF_TYPE_PRODUCTION_FIELD'),
            ),
            'UF_PARENT_ID_PRODUCT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PRODUCTION_ENTITY_UF_PARENT_ID_PRODUCT_FIELD'),
            ),
            'UF_COUNT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PRODUCTION_ENTITY_UF_COUNT_FIELD'),
            )
        );
    }
}