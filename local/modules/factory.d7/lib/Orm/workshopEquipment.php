<?php

namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class UfEquipmentTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> VALUE int mandatory
 * </ul>
 *
 * @package Bitrix\Workshop
 **/

class UfEquipmentTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_workshop_uf_equipment';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => Loc::getMessage('UF_EQUIPMENT_ENTITY_ID_FIELD'),
            ),
            'VALUE' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => Loc::getMessage('UF_EQUIPMENT_ENTITY_VALUE_FIELD'),
            ),
        );
    }
}