<?php
namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class TimeTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_TIME double optional
 * <li> UF_ID_EQUIPMENTS string optional
 * </ul>
 *
 * @package Bitrix\Transport
 **/

class TimeTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_transport_time';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('TIME_ENTITY_ID_FIELD'),
            ),
            'UF_TIME' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('TIME_ENTITY_UF_TIME_FIELD'),
            ),
            'UF_ID_EQUIPMENTS' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('TIME_ENTITY_UF_ID_EQUIPMENTS_FIELD'),
            ),
        );
    }
}