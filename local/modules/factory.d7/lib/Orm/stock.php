<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 16.03.2019
 * Time: 21:08
 */
namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class StockTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_NAME string optional
 * <li> UF_WORKSHOP int optional
 * <li> UF_BRANCH int optional
 * <li> UF_COORDINATE string optional
 * </ul>
 *
 * @package Bitrix\Stock
 **/

class StockTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_stock';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('STOCK_ENTITY_ID_FIELD'),
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('STOCK_ENTITY_UF_NAME_FIELD'),
            ),
            'UF_WORKSHOP' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('STOCK_ENTITY_UF_WORKSHOP_FIELD'),
            ),
            'UF_BRANCH' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('STOCK_ENTITY_UF_BRANCH_FIELD'),
            ),
            'UF_COORDINATE' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('STOCK_ENTITY_UF_COORDINATE_FIELD'),
            ),
        );
    }
}