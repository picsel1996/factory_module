<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 16.03.2019
 * Time: 21:08
 */
namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class CarrierTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_NAME string optional
 * <li> UF_LOAD_CAPACITY double optional
 * <li> UF_SPACIOUSNESS double optional
 * <li> UF_SPEED double optional
 * </ul>
 *
 * @package Bitrix\Type
 **/

class TypeCarrierTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_type_carrier';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('CARRIER_ENTITY_ID_FIELD'),
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('CARRIER_ENTITY_UF_NAME_FIELD'),
            ),
            'UF_LOAD_CAPACITY' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('CARRIER_ENTITY_UF_LOAD_CAPACITY_FIELD'),
            ),
            'UF_SPACIOUSNESS' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('CARRIER_ENTITY_UF_SPACIOUSNESS_FIELD'),
            ),
            'UF_SPEED' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('CARRIER_ENTITY_UF_SPEED_FIELD'),
            ),
        );
    }
}