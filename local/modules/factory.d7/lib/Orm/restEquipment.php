<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 16.03.2019
 * Time: 21:08
 */
namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class EquipmentTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_NAME string optional
 * <li> UF_EQUIPMENT int optional
 * <li> UF_TYPE_RESTRICTION int optional
 * <li> UF_VALUE double optional
 * </ul>
 *
 * @package Bitrix\Restriction
 **/

class RestEquipmentTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_restriction_equipment';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_ID_FIELD'),
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_UF_NAME_FIELD'),
            ),
            'UF_EQUIPMENT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_UF_EQUIPMENT_FIELD'),
            ),
            'UF_TYPE_RESTRICTION' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_UF_TYPE_RESTRICTION_FIELD'),
            ),
            'UF_VALUE' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('EQUIPMENT_ENTITY_UF_VALUE_FIELD'),
            ),
        );
    }
}