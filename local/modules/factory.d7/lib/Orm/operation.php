<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 16.03.2019
 * Time: 21:08
 */
namespace Factory;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class OperationTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_NAME string optional
 * <li> UF_TECH_PROC int optional
 * <li> UF_EQUIPMENT int optional
 * </ul>
 *
 * @package Bitrix\Operation
 **/

class OperationTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'fac_operation';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('OPERATION_ENTITY_ID_FIELD'),
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('OPERATION_ENTITY_UF_NAME_FIELD'),
            ),
            'UF_TECH_PROC' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('OPERATION_ENTITY_UF_TECH_PROC_FIELD'),
            ),
            'UF_EQUIPMENT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('OPERATION_ENTITY_UF_EQUIPMENT_FIELD'),
            ),
            'UF_TIME' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('OPERATION_ENTITY_UF_TIME_FIELD'),
            ),
        );
    }
}