<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 13.04.2019
 * Time: 21:17
 */

namespace Factory;

use Bitrix\Main\Config\Option;


class Calendar
{
    public function __construct()
    {
        $this->workTimeStart = (int)Option::get("calendar", "work_time_start");
        $this->workTimeEnd = (int)Option::get("calendar", "work_time_end");
        $this->weekHolidays = Option::get("calendar", "week_holidays");
        $this->yearHolidays = Option::get("calendar", "year_holidays");
    }

    public static function getNumberDayInWeek($day = 'time()')
    {
        return date('N', $day);
    }

    public function differenceTwoDays($firstDay, $secondDay)
    {
        $firstDay = self::getNumberDayInWeek($firstDay);
        $secondDay = self::getNumberDayInWeek($secondDay);
        return (int)(abs($firstDay - $secondDay));
    }

    public function isHoliday($day):bool
    {
        $holidaysInWeek = self::getHolidaysNumberDaysInWeekCompany();
        $arWeekHolidays = explode('|', $this->weekHolidays);
        $arYearHolidays = explode(',', $this->yearHolidays);
        if (
            in_array($holidaysInWeek[self::getNumberDayInWeek($day)], $arWeekHolidays) ||
            in_array(date('d.m', $day), $arYearHolidays)
        ) {
            return true;
        }
        return false;
    }

    public function getTimeStamp($date)
    {
        $date = date_parse_from_format('d.m.Y H:i', $date);
        $startDay = $date['day'];
        $startMonth = $date['month'];
        $startYear = $date['year'];
        if (!$date['hour'] && !$date['minute']) {
            $startHour = 0;
            $startMinute = 0;
        } else {
            $startHour = (int)$date['hour'];
            $startMinute = (int)$date['minute'];
        }
        $timestampStartDate = mktime($startHour, $startMinute, 0, $startMonth, $startDay, $startYear);
        return $timestampStartDate;
    }

    private static function getHolidaysNumberDaysInWeekCompany()
    {
        return [
            1 => 'MO',
            2 => 'TU',
            3 => 'WE',
            4 => 'TH',
            5 => 'FR',
            6 => 'SA',
            7 => 'SU'
        ];
    }
}