<?php
Bitrix\Main\Loader::registerAutoloadClasses(
    "factory.d7",
    [
        "Factory\\BranchTable" => "lib/Orm/branch.php",
        "Factory\\CarrierTable" => "lib/Orm/carrier.php",
        "Factory\\EquipmentTable" => "lib/Orm/equipment.php",
        "Factory\\OperationTable" => "lib/Orm/operation.php",
        "Factory\\ProcessTable" => "lib/Orm/process.php",
        "Factory\\ProductionTable" => "lib/Orm/production.php",
        "Factory\\RestEquipmentTable" => "lib/Orm/restEquipment.php",
        "Factory\\StockTable" => "lib/Orm/stock.php",
        "Factory\\TypeCarrierTable" => "lib/Orm/typeCarrier.php",
        "Factory\\TypeProductionTable" => "lib/Orm/typeProduction.php",
        "Factory\\TypeRestrictionEquipmentTable" => "lib/Orm/typeRestEquipment.php",
        "Factory\\WorkshopTable" => "lib/Orm/workshop.php",
        'Factory\\Factory' => 'lib/Factory.php',
        'Factory\\Calendar' => 'lib/Calendar.php',
        'Factory\\UfEquipmentTable' => 'lib/Orm/workshopEquipment.php',
        'Factory\\TimeTable' => 'lib/Orm/transportTime.php'
    ]
);