<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 24.03.2019
 * Time: 18:35
 */
CModule::IncludeModule('highloadblock');
$result = Bitrix\Highloadblock\HighloadBlockTable::add([
    'NAME' => 'Stock',//должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'fac_stock',//должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    'LANGS' => [
        'ru' => 'Склада',
        'en' => 'Stock'
    ]
]);

if ($result->isSuccess()) {
    $highLoadBlockId = $result->getId();
    $arUserTypeData = [
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_NAME',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_WORKSHOP',
            'USER_TYPE_ID' => 'hlblock',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'HLBLOCK_ID' => $arIdPropertyNames['fac_workshop']['ID_HL'],
                'HLFIELD_ID' => $arIdPropertyNames['fac_workshop']['ID_PROPERTY_NAME'],
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Цех',
                'en' => 'Workshop',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Цех',
                'en' => 'Workshop',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Цех',
                'en' => 'Workshop',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_BRANCH',
            'USER_TYPE_ID' => 'hlblock',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'HLBLOCK_ID' => $arIdPropertyNames['fac_branch']['ID_HL'],
                'HLFIELD_ID' => $arIdPropertyNames['fac_branch']['ID_PROPERTY_NAME'],
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Филиал',
                'en' => 'Branch',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Филиал',
                'en' => 'Branch',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Филиал',
                'en' => 'Branch',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_COORDINATE',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Местоположение',
                'en' => 'Location',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Местоположение',
                'en' => 'Location',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Местоположение',
                'en' => 'Location',
            ]
        ],
    ];

    foreach ($arUserTypeData as $userTypeData) {
        $userTypeEntity = new CUserTypeEntity();
        $userTypeId = $userTypeEntity->Add($userTypeData);
        if ($userTypeData['FIELD_NAME'] == 'UF_NAME') {
            $arIdPropertyNames['fac_workshop'] = [
                'ID_HL' => $highLoadBlockId,
                'ID_PROPERTY_NAME' => $userTypeId
            ];
        }
    }
}