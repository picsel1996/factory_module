<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 24.03.2019
 * Time: 18:35
 */
CModule::IncludeModule('highloadblock');
$result = Bitrix\Highloadblock\HighloadBlockTable::add([
    'NAME' => 'Production',//должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'fac_production',//должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    'LANGS' => [
        'ru' => 'Объекты производства',
        'en' => 'Production'
    ]
]);

if ($result->isSuccess()) {
    $highLoadBlockId = $result->getId();
    $arUserTypeData = [
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_NAME',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_COUNT',
            'USER_TYPE_ID' => 'integer',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Количество',
                'en' => 'Count',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Количество',
                'en' => 'Count',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Количество',
                'en' => 'Count',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_TYPE_PRODUCTION',
            'USER_TYPE_ID' => 'hlblock',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'HLBLOCK_ID' => $arIdPropertyNames['fac_type_production']['ID_HL'],
                'HLFIELD_ID' => $arIdPropertyNames['fac_type_production']['ID_PROPERTY_NAME'],
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Тип объекта производства',
                'en' => 'Type Production',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Тип объекта производства',
                'en' => 'Type Production',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Тип объекта производства',
                'en' => 'Type Production',
            ]
        ]
    ];

    foreach ($arUserTypeData as $userTypeData) {
        $userTypeEntity = new CUserTypeEntity();
        $userTypeId = $userTypeEntity->Add($userTypeData);
        if ($userTypeData['FIELD_NAME'] == 'UF_NAME') {
            $arIdPropertyNames['fac_production'] = [
                'ID_HL' => $highLoadBlockId,
                'ID_PROPERTY_NAME' => $userTypeId
            ];
        }
    }

    $arUserTypeData = [
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_PARENT_ID_PRODUCT',
            'USER_TYPE_ID' => 'hlblock',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'HLBLOCK_ID' => $arIdPropertyNames['fac_production2']['ID_HL'],
                'HLFIELD_ID' => $arIdPropertyNames['fac_production2']['ID_PROPERTY_NAME'],
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Родительский объект',
                'en' => 'Parent production',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Родительский объект',
                'en' => 'Parent production',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Родительский объект',
                'en' => 'Parent production',
            ]
        ]
    ];


    foreach ($arUserTypeData as $userTypeData) {
        $userTypeEntity = new CUserTypeEntity();
        $userTypeId = $userTypeEntity->Add($userTypeData);
        if ($userTypeData['FIELD_NAME'] == 'UF_NAME') {
            $arIdPropertyNames['fac_production2'] = [
                'ID_HL' => $highLoadBlockId,
                'ID_PROPERTY_NAME' => $userTypeId
            ];
        }
    }

}