<?php
CModule::IncludeModule('highloadblock');
$result = Bitrix\Highloadblock\HighloadBlockTable::add([
    'NAME' => 'TransportTime',//должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'fac_transport_time',//должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    'LANGS' => [
        'ru' => 'Время транспортировки',
        'en' => 'Transport Time'
    ]
]);

if ($result->isSuccess()) {
    $highLoadBlockId = $result->getId();
    $arUserTypeData = [
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_TIME',
            'USER_TYPE_ID' => 'float',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Время',
                'en' => 'Time',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Время',
                'en' => 'Time',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Время',
                'en' => 'Time',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_ID_EQUIPMENTS',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipments',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipments',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipments',
            ]
        ],
    ];

    foreach ($arUserTypeData as $userTypeData) {
        $userTypeEntity = new CUserTypeEntity();
        $userTypeId = $userTypeEntity->Add($userTypeData);
        if ($userTypeData['FIELD_NAME'] == 'UF_NAME') {
            $arIdPropertyNames['fac_transport_time'] = [
                'ID_HL' => $highLoadBlockId,
                'ID_PROPERTY_NAME' => $userTypeId
            ];
        }
    }
}