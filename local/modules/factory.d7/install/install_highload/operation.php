<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 24.03.2019
 * Time: 18:35
 */

CModule::IncludeModule('highloadblock');
$result = Bitrix\Highloadblock\HighloadBlockTable::add([
    'NAME' => 'Operations',//должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'fac_operation',//должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    'LANGS' => [
        'ru' => 'Операции',
        'en' => 'Operations'
    ]
]);

if ($result->isSuccess()) {
    $highLoadBlockId = $result->getId();
    $arUserTypeData = [
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_NAME',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_TIME',
            'USER_TYPE_ID' => 'double',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Длительность операции',
                'en' => 'Time operation',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Длительность операции',
                'en' => 'Time operation',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Длительность операции',
                'en' => 'Time operation',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_EQUIPMENT',
            'USER_TYPE_ID' => 'hlblock',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'HLBLOCK_ID' => $arIdPropertyNames['fac_equipment']['ID_HL'],
                'HLFIELD_ID' => $arIdPropertyNames['fac_equipment']['ID_PROPERTY_NAME'],
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipment',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipment',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipment',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_TECH_PROC',
            'USER_TYPE_ID' => 'hlblock',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'HLBLOCK_ID' => $arIdPropertyNames['fac_tech_process']['ID_HL'],
                'HLFIELD_ID' => $arIdPropertyNames['fac_tech_process']['ID_PROPERTY_NAME'],
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Технологический процесс',
                'en' => 'Technological process',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Технологический процесс',
                'en' => 'Technological process',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Технологический процесс',
                'en' => 'Technological process',
            ]
        ]
    ];

    foreach ($arUserTypeData as $userTypeData) {
        $userTypeEntity = new CUserTypeEntity();
        $userTypeId = $userTypeEntity->Add($userTypeData);
        if ($userTypeData['FIELD_NAME'] == 'UF_NAME') {
            $arIdPropertyNames['fac_operation'] = [
                'ID_HL' => $highLoadBlockId,
                'ID_PROPERTY_NAME' => $userTypeId
            ];
        }
    }
}