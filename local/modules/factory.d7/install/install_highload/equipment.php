<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 24.03.2019
 * Time: 18:35
 */

CModule::IncludeModule('highloadblock');
$result = Bitrix\Highloadblock\HighloadBlockTable::add([
    'NAME' => 'Equipment',//должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'fac_equipment',//должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    'LANGS' => [
        'ru' => 'Оборудование',
        'en' => 'Equipment'
    ]
]);

if ($result->isSuccess()) {
    $highLoadBlockId = $result->getId();
    $arUserTypeData = [
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_NAME',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_DESCRIPTION',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Описание',
                'en' => 'Description',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Описание',
                'en' => 'Description',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Описание',
                'en' => 'Description',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_COUNT',
            'USER_TYPE_ID' => 'integer',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Количество',
                'en' => 'Count',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Количество',
                'en' => 'Count',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Количество',
                'en' => 'Count',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_COORDINATE',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Местоположение',
                'en' => 'Location',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Местоположение',
                'en' => 'Location',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Местоположение',
                'en' => 'Location',
            ]
        ],
    ];

    foreach ($arUserTypeData as $userTypeData) {
        $userTypeEntity = new CUserTypeEntity();
        $userTypeId = $userTypeEntity->Add($userTypeData);
        if ($userTypeData['FIELD_NAME'] == 'UF_NAME') {
            $arIdPropertyNames['fac_equipment'] = [
                'ID_HL' => $highLoadBlockId,
                'ID_PROPERTY_NAME' => $userTypeId
            ];
        }
    }
}