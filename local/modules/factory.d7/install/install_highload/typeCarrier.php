<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 24.03.2019
 * Time: 18:35
 */
CModule::IncludeModule('highloadblock');
$result = Bitrix\Highloadblock\HighloadBlockTable::add([
    'NAME' => 'TypeCarrier',//должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'fac_type_carrier',//должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    'LANGS' => [
        'ru' => 'Тип перевозчика',
        'en' => 'Type carrier'
    ]
]);

if ($result->isSuccess()) {
    $highLoadBlockId = $result->getId();
    $arUserTypeData = [
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_NAME',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_LOAD_CAPACITY',
            'USER_TYPE_ID' => 'float',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Грузоподъемность',
                'en' => 'Load capacity',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Грузоподъемность',
                'en' => 'Load capacity',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Грузоподъемность',
                'en' => 'Load capacity',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_SPACIOUSNESS',
            'USER_TYPE_ID' => 'float',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Вместительность',
                'en' => 'Spaciousness',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Вместительность',
                'en' => 'Spaciousness',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Вместительность',
                'en' => 'Spaciousness',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_SPEED',
            'USER_TYPE_ID' => 'float',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Скорость',
                'en' => 'Speed',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Скорость',
                'en' => 'Speed',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Скорость',
                'en' => 'Speed',
            ]
        ],
    ];

    foreach ($arUserTypeData as $userTypeData) {
        $userTypeEntity = new CUserTypeEntity();
        $userTypeId = $userTypeEntity->Add($userTypeData);
        if ($userTypeData['FIELD_NAME'] == 'UF_NAME') {
            $arIdPropertyNames['fac_type_carrier'] = [
                'ID_HL' => $highLoadBlockId,
                'ID_PROPERTY_NAME' => $userTypeId
            ];
        }
    }
}