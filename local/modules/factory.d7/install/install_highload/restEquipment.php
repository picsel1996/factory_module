<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 24.03.2019
 * Time: 18:35
 */
CModule::IncludeModule('highloadblock');
$result = Bitrix\Highloadblock\HighloadBlockTable::add([
    'NAME' => 'RestrictionEquipment',//должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
    'TABLE_NAME' => 'fac_restriction_equipment',//должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    'LANGS' => [
        'ru' => 'Ограничения оборудования',
        'en' => 'Restriction Equipment'
    ]
]);

if ($result->isSuccess()) {
    $highLoadBlockId = $result->getId();
    $arUserTypeData = [
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_NAME',
            'USER_TYPE_ID' => 'string',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Названиеа',
                'en' => 'Name',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_EQUIPMENT',
            'USER_TYPE_ID' => 'hlblock',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'HLBLOCK_ID' => $arIdPropertyNames['fac_equipment']['ID_HL'],
                'HLFIELD_ID' => $arIdPropertyNames['fac_equipment']['ID_PROPERTY_NAME'],
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipment',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipment',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Оборудование',
                'en' => 'Equipment',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_TYPE_RESTRICTION',
            'USER_TYPE_ID' => 'hlblock',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'HLBLOCK_ID' => $arIdPropertyNames['fac_type_restriction_equipment']['ID_HL'],
                'HLFIELD_ID' => $arIdPropertyNames['fac_type_restriction_equipment']['ID_PROPERTY_NAME'],
            ],
            'EDIT_FORM_LABEL' => [
                'ru' => 'Тип ограничения оборудования',
                'en' => 'Type restriction',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Тип ограничения оборудования',
                'en' => 'Type restriction',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Тип ограничения оборудования',
                'en' => 'Type restriction',
            ]
        ],
        [
            'ENTITY_ID' => 'HLBLOCK_' . $highLoadBlockId,
            'FIELD_NAME' => 'UF_VALUE',
            'USER_TYPE_ID' => 'double',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => '',
            'EDIT_IN_LIST' => '',
            'IS_SEARCHABLE' => 'N',
            'EDIT_FORM_LABEL' => [
                'ru' => 'Значение',
                'en' => 'Value',
            ],
            'LIST_COLUMN_LABEL' => [
                'ru' => 'Значение',
                'en' => 'Value',
            ],
            'LIST_FILTER_LABEL' => [
                'ru' => 'Значение',
                'en' => 'Value',
            ]
        ],
    ];

    foreach ($arUserTypeData as $userTypeData) {
        $userTypeEntity = new CUserTypeEntity();
        $userTypeId = $userTypeEntity->Add($userTypeData);
        if ($userTypeData['FIELD_NAME'] == 'UF_NAME') {
            $arIdPropertyNames['fac_type_production'] = [
                'ID_HL' => $highLoadBlockId,
                'ID_PROPERTY_NAME' => $userTypeId
            ];
        }
    }
}