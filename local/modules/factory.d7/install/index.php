<?php

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

class Factory_D7 extends CModule
{
    //важен порядок подключения!
    static $highloads = [
        'install_highload/typeProduction.php',
        'install_highload/equipment.php',
        'install_highload/branch.php',
        'install_highload/typeCarrier.php',
        'install_highload/typeRestEquipment.php',
        'install_highload/restEquipment.php',
        'install_highload/workshop.php',
        'install_highload/stock.php',
        'install_highload/carrier.php',
        'install_highload/production.php',
        'install_highload/process.php',
        'install_highload/operation.php',
        'install_highload/transportTime.php',
    ];

    function __construct()
    {
        $this->MODULE_ID = "factory.d7";
        $this->MODULE_VERSION = "1.0.0";
        $this->MODULE_VERSION_DATE = "20.03.2016";
        $this->MODULE_NAME = "Модуль расчета производственного цикла";
        $this->MODULE_DESCRIPTION = "Расчет времени изготовления объектов производства. Используется на корпоративных порталах производств.";
    }

    public function DoInstall()
    {
        ModuleManager::registerModule('factory.d7');
        $this->installDB();
    }

    public function DoUninstall()
    {
        $this->uninstallDB();
        ModuleManager::unRegisterModule('factory.d7');
    }

    public function installDB()
    {
        if (Loader::includeModule('factory.d7')) {
            $arIdPropertyNames = [];
            foreach (self::$highloads as $highload) {
                require_once __DIR__ . '/' . $highload;
            }
            return true;
        }
    }

    public function uninstallDB()
    {
        if (Loader::includeModule('factory.d7')) {
            CModule::IncludeModule('highloadblock');
            $hlblockId = Bitrix\Highloadblock\HighloadBlockTable::getList([
                'select' => ['ID'],
                'filter' => ['TABLE_NAME' => 'fac_%']
            ])->fetchAll();

            foreach ($hlblockId as $id) {
                Bitrix\Highloadblock\HighloadBlockTable::delete($id);
            }

            return true;
        }
    }
}
