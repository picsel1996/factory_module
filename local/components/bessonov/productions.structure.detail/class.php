<?php

/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:13
 */

use Factory\Factory,
    Factory\EquipmentTable,
    Factory\WorkshopTable,
    Factory\UfEquipmentTable;

class CStructureDetail extends CBitrixComponent implements \Bitrix\Main\Engine\Contract\Controllerable
{
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    public function executeComponent()
    {
        $object = new Factory($this->arParams['ID_WORKSHOP'], 'structural_object');
        $this->arResult['WORKSHOP'] = $object->workshops[0];
        $this->arResult['BRANCH'] = $object->branch[0];
        if (count($this->arResult['WORKSHOP']['UF_EQUIPMENT']) > 0) {
            $this->arResult['EQUIPMENT'] = $object::getEquipment($this->arResult['WORKSHOP']['UF_EQUIPMENT']);
        }
        $this->includeComponentTemplate();
    }

    public function ajaxSaveEquipmentDataAction($post)
    {
        $result = 'unsuccess';
        if ($post['UF_WORKSHOP'] ?? false) {
            $idWorkshop = $post['UF_WORKSHOP'];
            unset($post['UF_WORKSHOP']);
            $equipments = EquipmentTable::getList(['select' => ['ID'], 'filter' => $post])->fetchAll();
            if (count($equipments) == 0) {
                EquipmentTable::add($post);
                $idEquipment = EquipmentTable::getList(['select' => ['ID'], 'filter' => $post])->fetchAll()[0]['ID'];
            } else {
                $idEquipment = $equipments[0]['ID'];
            }
            $workshop = new Factory($idWorkshop, 'structural_object');
            $equipments = $workshop->workshops[0]['UF_EQUIPMENT'];
            if (!array_search($idEquipment, $equipments)) {
                $ufEquipment = $this->pushMultiData($equipments, $idEquipment);
                $result = WorkshopTable::update($idWorkshop, ['UF_EQUIPMENT' => $ufEquipment]);
//            $idUfEquipment = UfEquipmentTable::getList([
//                'select' => ['ID'],
//                'filter' => ['ID' => $idWorkshop]
//            ])->fetchAll();
//            UfEquipmentTable::delete($idUfEquipment);
//            foreach ($equipments as $item) {
//                UfEquipmentTable::add(['ID' => $idUfEquipment, 'VALUE' => $item]);
//            }
            }
        } else {
            $idEquipment = $post['ID'];
            unset($post['ID']);
            $result = EquipmentTable::update($idEquipment, [
                'UF_COUNT' => $post['UF_COUNT'],
                'UF_COORDINATE' => $post['UF_COORDINATE'],
                'UF_NAME' => $post['UF_NAME'],
                'UF_DESCRIPTION' => $post['UF_DESCRIPTION'],
            ]);
        }
        return $post;
    }

    public function ajaxDeleteEquipmentAction($post)
    {
        $workshop = new Factory($post['ID_WORKSHOP'], 'structural_object');
        $equipments = $workshop->workshops[0]['UF_EQUIPMENT'];
        if ($key = array_search($post['ID_EQUIPMENT'], $equipments)) {
            unset($equipments[$key]);
            $ufEquipment = $this->pushMultiData($equipments);
            WorkshopTable::update($post['ID_WORKSHOP'], ['UF_EQUIPMENT' => $ufEquipment]);
        }
        EquipmentTable::delete($post['ID_EQUIPMENT']);
        return $equipments;
    }

    private function pushMultiData($arData, $newData = false)
    {
        $string = '';
        if ($newData) {
            $arData[] = $newData;
        }
        if (count($arData) > 0) {
            $string = 'a:' . count($arData) . ':{';
            foreach ($arData as $key => $data) {
                $string .= 'i:' . $key . ';s:1:"' . $data . '";';
            }
            $string .= '}';
        }
        return $string;
    }

    public function configureActions()
    {
        return [];
    }


}