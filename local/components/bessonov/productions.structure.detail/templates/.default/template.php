<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:18
 */

$APPLICATION->SetTitle($arResult['WORKSHOP']['UF_NAME'] . ' филиала ' . $arResult['BRANCH']['UF_NAME']);
?>
    <script src="https://api-maps.yandex.ru/2.1/?apikey=20738e30-92e1-48ff-9637-63a543860671&lang=ru_RU" type="text/javascript"></script>
    <script>console.log(<?=json_encode($arResult)?>)</script>
    <script>console.log(<?=json_encode(distance(55.6855,39.3483,55.1855,39.3403))?>)</script>
<?
\Bitrix\Main\UI\Extension::load("ui.buttons");
function distance($lat1,$lng1,$lat2,$lng2) //(x1,y1,x2,y2)
{
    $lat1  = deg2rad($lat1);
    $lng1  = deg2rad($lng1);
    $lat2  = deg2rad($lat2);
    $lng2  = deg2rad($lng2);
    $delta_lat  = ($lat2 - $lat1);
    $delta_lng  = ($lng2 - $lng1);
    return round( 6378137 * acos( cos( $lat1 ) * cos( $lat2 ) * cos( $lng1 - $lng2 ) + sin( $lat1 ) * sin( $lat2 ) ) );
}
if ($arResult ?? false) {
    $list = [];
    foreach ($arResult['EQUIPMENT'] as $key => $equipment) {
        $list[] = [
            'data' => [ //Данные ячеек
                "NUMBER" => ++$key,
                "UF_NAME" => $equipment['UF_NAME'],
                "UF_DESCRIPTION" => $equipment['UF_DESCRIPTION'],
                "UF_COUNT" => $equipment['UF_COUNT']
            ],
            'actions' => [ //Действия над ними
                [
                    'text' => 'Редактировать',
                    'onclick' => 'editEquipments(' . $equipment['ID'] . ')'
                ],
                [
                    'text' => 'Удалить',
                    'onclick' => 'deleteEquipments(' . $equipment['ID'] . ',' . $arResult['WORKSHOP']['ID'] . ')'
                ]
            ],
            'id' => $equipment['ID']
                . '" data-name = "' . $equipment['UF_NAME']
                . '" data-quantity = "' . $equipment['UF_COUNT']
                . '" data-description = "' . $equipment['UF_DESCRIPTION']
                . '" data-coordinate = "' . $equipment['UF_COORDINATE'],
        ];
    }
}
$this->SetViewTarget('pagetitle'); ?>
    <div class="ui-btn-double ui-btn-primary tasks-interface-filter-btn-add">
        <a class="ui-btn-main" id="equipment-buttonAdd" onclick="addNewEquipment(<?= $arResult['WORKSHOP']['ID'] ?>)">Добавить
            Оборудование</a>
    </div>
<?php
$this->EndViewTarget();

$APPLICATION->IncludeComponent(
    "bitrix:main.ui.grid",
    "",
    [
        'GRID_ID' => 'grid_equipments_object',
        "COLUMNS" => [
            ["id" => "NUMBER", "name" => '№', 'sort' => 'NUMBER', 'default' => true],
            ["id" => "UF_NAME", "name" => 'Оборудование', 'sort' => 'UF_NAME', 'default' => true],
            ["id" => "UF_DESCRIPTION", "name" => 'Описание', 'sort' => 'UF_DESCRIPTION', 'default' => true],
            ["id" => "UF_COUNT", "name" => 'Количество', 'sort' => 'UF_COUNT', 'default' => true],
        ],
        "ROWS" => $list,
        'AJAX_MODE' => 'Y',
        'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', 'grid_equipments_object'),
        'ALLOW_COLUMNS_SORT' => true,
        'ALLOW_COLUMNS_RESIZE' => true,
        'ALLOW_HORIZONTAL_SCROLL' => true,
        'ALLOW_SORT' => true,
        'ALLOW_PIN_HEADER' => true,
        'AJAX_OPTION_HISTORY' => 'N'
    ]
);
CJSCore::Init(['popup']);
?>