$(function () {


});
var coords = 0;

function addYaMap(idMap, object) {
    console.log(object);
    ymaps.ready(init);
    function init() {
        // Создание карты.
        var myMap = new ymaps.Map(idMap, {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [55.76, 37.64],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.
            zoom: 7
        });
        if (object.coordinate) {
            coords = object.coordinate;
            myGeoObject = new ymaps.GeoObject({
                // Описание геометрии.
                geometry: {
                    type: "Point",
                    coordinates: object.coordinate.split(',')
                },
                // Свойства.
                properties: {
                    // Контент метки.
                    // iconContent: 'Я тащусь',
                    hintContent: object.name
                }
            }, {
                // Опции.
                // Иконка метки будет растягиваться под размер ее содержимого.
                preset: 'islands#blackStretchyIcon',
                // Метку можно перемещать.
                draggable: true
            });
            myMap.geoObjects.add(myGeoObject);
            myGeoObject.events.add('dragend', function (e) {
                coords = myGeoObject.geometry.getCoordinates();
                coords = coords[0].toPrecision(6) + ',' + coords[1].toPrecision(6);
            });
        }
        myMap.events.add('click', function (e) {
            $('#map-' + object.id).children('ymaps').find('ymaps[class*="-placemark-overlay"]').remove();
            // if ($('#map-' + object.id).children('ymaps').find('ymaps[class*="-placemark-overlay"]').length === 0) {
            coords = e.get('coords');
            myGeoObject = new ymaps.GeoObject({
                // Описание геометрии.
                geometry: {
                    type: "Point",
                    coordinates: [coords[0].toPrecision(6), coords[1].toPrecision(6)]
                },
                // Свойства.
                properties: {
                    // Контент метки.
                    // iconContent: 'Я тащусь',
                    hintContent: object.name
                }
            }, {
                // Опции.
                // Иконка метки будет растягиваться под размер ее содержимого.
                preset: 'islands#blackStretchyIcon',
                // Метку можно перемещать.
                draggable: true
            });
            myMap.geoObjects.add(myGeoObject);
            // }
            coords = coords[0].toPrecision(6) + ',' + coords[1].toPrecision(6);
        });

        // Обработка события, возникающего при щелчке
        // правой кнопки мыши в любой точке карты.
        // При возникновении такого события покажем всплывающую подсказку
        // в точке щелчка.
        // myMap.events.add('contextmenu', function (e) {
        //     myMap.hint.open(e.get('coords'), 'Кто-то щелкнул правой кнопкой');
        // });

        // Скрываем хинт при открытии балуна.
        // myMap.events.add('balloonopen', function (e) {
        //     myMap.hint.close();
        // });
    }
}


function editEquipments(idEquipment) {
    var data = $('tr.main-grid-row.main-grid-row-body[data-id="' + idEquipment + '"]');
    var object = {
        id: idEquipment,
        name: data.attr('data-name'),
        quantity: data.attr('data-quantity'),
        description: data.attr('data-description'),
        coordinate: data.attr('data-coordinate')
    };

    var content = '<div class="container-fluid">\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Название</div>\n' +
        '        <div class="col-xs-6">' +
        '            <input id="name" type="text" placeholder="Название" value="' + object.name + '" class="form-control input-md">' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Описание</div>\n' +
        '        <div class="col-xs-6">' +
        '            <input id="description" type="text" placeholder="Описание" value="' + object.description + '" class="form-control input-md">' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Количество</div>\n' +
        '        <div class="col-xs-6">' +
        '            <input id="quantity" type="number" placeholder="Количество" value="' + object.quantity + '" class="form-control input-md">' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Местоположение</div>\n' +
        '        <div class="col-xs-6">' +
        '            <div id="map-' + idEquipment + '" style="width: 400px; height: 300px"></div>' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
    editElement(object, content)

}

function addNewEquipment(idWorkshop) {
    var content = '<div class="container-fluid">\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Название</div>\n' +
        '        <div class="col-xs-6">' +
        '            <input id="name" type="text" placeholder="Название" value="" class="form-control input-md">' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Описание</div>\n' +
        '        <div class="col-xs-6">' +
        '            <input id="description" type="text" placeholder="Описание" value="" class="form-control input-md">' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Количество</div>\n' +
        '        <div class="col-xs-6">' +
        '            <input id="quantity" type="number" placeholder="Количество" value="" class="form-control input-md">' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Местоположение</div>\n' +
        '        <div class="col-xs-6">' +
        '            <div id="map-new" style="width: 400px; height: 300px"></div>' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
    addElement(content, idWorkshop);
}

function deleteEquipments(idEquipment, idWorkshop) {
    callMethodByAjax(
        'bessonov:productions.structure.detail',
        'ajaxDeleteEquipment',
        {
            ID_EQUIPMENT: idEquipment,
            ID_WORKSHOP: idWorkshop
        }
    ).then(function () {
        reloadGrid('grid_equipments_object');
    });
}

//формы редактирования
function editElement(object, content) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-edition_" + object.id, BX('element'), {
        content: content,
        width: 900, // ширина окна
        height: 400, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Редактирование :' + object.name,
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-' + object.id, // идентификатор
                idOperation: object.id,
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-edition_' + object.id).find('div.container-fluid');
                        var data = {
                            ID: object.id,
                            UF_NAME: formData.find('input#name').val(),
                            UF_DESCRIPTION: formData.find('input#description').val(),
                            UF_COUNT: parseInt(formData.find('input#quantity').val()),
                            UF_COORDINATE: coords,
                        };
                        console.log(data);
                        callMethodByAjax(
                            'bessonov:productions.structure.detail',
                            'ajaxSaveEquipmentData',
                            data
                        ).then(function () {
                            popup.close();
                            reloadGrid('grid_equipments_object');
                        });
                    }
                }
            })
        ],
        events: {
            onPopupShow: function () {
                // Событие при показе окна
            },
            onPopupClose: function () {
                // Событие при закрытии окна
            }
        }
    });
    popup.show();
    if ($('#map-' + object.id).children('ymaps').length === 0) {
        addYaMap('map-' + object.id, object);
    }

}

//формы добавления
function addElement(content, idWorkshop) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-add-equipment", BX('element'), {
        content: content,
        width: 900, // ширина окна
        height: 400, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Добавление нового оборудования',
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-add-equipment', // идентификатор
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-add-equipment').find('div.container-fluid');

                        var data = {
                            UF_WORKSHOP: idWorkshop,
                            UF_NAME: formData.find('input#name').val(),
                            UF_DESCRIPTION: formData.find('input#description').val(),
                            UF_COUNT: formData.find('input#quantity').val(),
                            UF_COORDINATE: coords,
                        };
                        callMethodByAjax(
                            'bessonov:productions.structure.detail',
                            'ajaxSaveEquipmentData',
                            data
                        ).then(function (result) {
                            popup.close();
                            reloadGrid('grid_equipments_object');
                            console.log(result);
                        });
                    }
                }
            })
        ],
        events: {
            onPopupShow: function () {
                // Событие при показе окна
            },
            onPopupClose: function () {
                // Событие при закрытии окна
            }
        }
    });
    popup.show();
    if ($('#map-new').children('ymaps').length === 0) {
        var formData = $('div#popup-add-equipment').find('div.container-fluid');

        var object = {
            id: 'new',
            name: formData.find('input#name').val(),
        };
        addYaMap('map-new', object);
    }
}

//отправка аякс
function callMethodByAjax(component, method, data) {
    return BX.ajax.runComponentAction(component,
        method, { // Вызывается без постфикса Action
            mode: 'class',
            data: {
                post: data
            }, // ключи объекта data соответствуют параметрам метода
        });
}

function reloadGrid(idGrid) {
    var reloadParams = {apply_filter: 'Y', clear_nav: 'Y'};
    var gridObject = BX.Main.gridManager.getById(idGrid);
    gridObject.instance.reloadTable('POST', reloadParams);
}