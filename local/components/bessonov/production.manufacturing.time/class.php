<?php

/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:13
 */

use Factory\Factory,
    Factory\EquipmentTable,
    Factory\OperationTable,
    Factory\ProcessTable,
    Factory\Calendar,
    Factory\TimeTable;

class CObjectManufacturingTime extends CBitrixComponent implements \Bitrix\Main\Engine\Contract\Controllerable
{
    public function executeComponent()
    {
        $object = new Factory($this->arParams['PRODUCT_ID']);
        $this->arResult['NAME_OBJECT'] = $object->nameObject;
        $this->arResult['ID_OBJECT'] = $object->idObject;
        $this->arResult['IS_FINAL_OBJECT'] = $object->isFinalObject($object->idObject);
        $this->arResult['QUANTITY_OBJECT'] = $object->quantityObject;
        $this->arResult['ID_TECHPROCES'] = $object->techProcesObject['ID'];
        $this->arResult['OPERATIONS'] = $object->operationsObject;
        $idEquipment = array_unique(array_column($this->arResult['OPERATIONS'], 'UF_EQUIPMENT'));
        $this->arResult['EQUIPMENTS'] = $this->getEquipmentOperations($idEquipment);
        $this->arResult['TREE_OBJECT'] = $object->treeObject;
        $recursive = ($object->treeObject[array_keys($object->treeObject)[0]]['HAVE_CHILD']) ? true : false;
        $this->arResult['SEQUENTIALLY_TIME'] = $this->calculationSequentiallyTimeManufacturingForward(
            new Factory($this->arParams['PRODUCT_ID']),
            $this->arResult['EQUIPMENTS']
        );
        $this->arResult['PARALLEL_TIME'] = $this->calculationParallelTimeManufacturing(
            new Factory($this->arParams['PRODUCT_ID']),
            $this->arResult['EQUIPMENTS']
        );
        $this->arResult['MIXED_TIME'] = $this->calculationMixedTimeManufacturing(
            new Factory($this->arParams['PRODUCT_ID']),
            $this->arResult['EQUIPMENTS']
        );
        $this->arResult['WORK_TIME_COMPANY'] = $this->getWorkTimeCompany();
        $this->arResult['EQUIPMENTS'] = $this->pushTimeUseToArrayEquipment($object, $this->arResult['EQUIPMENTS']);
        if ($recursive) {
            $timeManufacturingChainObject = [];
            $arrTimeStart = [];
            $arrChainMaxLength = [];
            $this->fullCalculationTimeManufacturing($object, $timeManufacturingChainObject, $arrTimeStart, $arrChainMaxLength);
            $this->arResult['TIME_MANUFACTURING_MAIN_CHAIN_OBJECT'] = $arrChainMaxLength;
            $this->arResult['TIME_MANUFACTURING_CHAIN_OBJECT'] = $timeManufacturingChainObject;
        }

        $this->includeComponentTemplate();
    }

    private function createCorrectChainForCircleGraph(&$timeManufacturingChainObject, $arrChainMaxLength)
    {

        foreach ($timeManufacturingChainObject as $id => &$item) {
            $item['START_TIME_FOR_GRAPH'] += array_sum($arrChainMaxLength);
            $item['END_TIME_FOR_GRAPH'] += array_sum($arrChainMaxLength);
        }
    }

    private function fullCalculationTimeManufacturing($object, &$time = [], &$arrTimeStart = [], &$arrChainMaxLength = [])
    {
        if (!empty($object->treeObject)) {
            foreach ($object->treeObject as $id => $children) {
                if (is_int($id) && empty($time[$id])) {
                    $childrenObject = $object;
                    $childrenObject->idObject = $id;
                    $childrenObject->nameObject = $children['NAME'];
                    $childrenObject->operationsObject = $children['TECH_PROCESS']['OPERATIONS'];
                    $childrenObject->quantityObject = $children['QUANTITY'] ?? $object->treeObject['QUANTITY'];
                    $childrenObject->treeObject = $children;
                    $idEquipment = array_unique(array_column($childrenObject->operationsObject, 'UF_EQUIPMENT'));
                    $equipments = $this->getEquipmentOperations($idEquipment);

                    $this->allTypeManufacturingCalculationBlock($object, $equipments, $time, -array_sum($arrTimeStart['TIME']));
                    $arrTimeStart['TIME'][$object->idObject] = $time[$object->idObject]['MIN_TIME'];
                    $arrTimeStart['ELEMENTS'][] = $object->idObject;

                    $this->fullCalculationTimeManufacturing($childrenObject, $time, $arrTimeStart, $arrChainMaxLength);
//                    array_pop($arrTimeStart);
                    array_pop($arrTimeStart['TIME']);
                    array_pop($arrTimeStart['ELEMENTS']);
                }
            }
        }
        if (array_sum($arrChainMaxLength['TIME']) < array_sum($arrTimeStart['TIME'])) {
            $arrChainMaxLength['ELEMENTS'] = $arrTimeStart['ELEMENTS'];
            $arrChainMaxLength['TIME'] = $arrTimeStart['TIME'];
        }
    }

    private function fullCalculationCalendarTimeManufacturing($object, &$time = [], &$arrDateStart = [], $postDataForPlaning = [], &$dateEnd = 0, &$dateStart = 0, $idEnd = 0, $forward = true)
    {
        $typeManufacturing = 'SEQUENTIALLY';
        $batch = 1;
        $calendar = new Calendar();
        if ($postDataForPlaning['MANUFACTURING_MAIN_CHAIN_OBJECT']) {
            if ($forward) {
                $postDataForPlaning['MANUFACTURING_MAIN_CHAIN_OBJECT']['ELEMENTS'] = array_reverse($postDataForPlaning['MANUFACTURING_MAIN_CHAIN_OBJECT']['ELEMENTS']);
            }
            foreach ($postDataForPlaning['MANUFACTURING_MAIN_CHAIN_OBJECT']['ELEMENTS'] as $id) {
                $childrenObject = $object;
                $childrenObject->idObject = $id;
                $childrenObject->nameObject = $postDataForPlaning['MANUFACTURING_CHAIN_OBJECT'][$id]['NAME_OBJECT'];
                $childrenObject->operationsObject = $postDataForPlaning['MANUFACTURING_CHAIN_OBJECT'][$id]['OPERATIONS'];
                $childrenObject->quantityObject = $postDataForPlaning['MANUFACTURING_CHAIN_OBJECT'][$id]['QUANTITY_OBJECT'];

                $typeManufacturing = $postDataForPlaning['MANUFACTURING_CHAIN_OBJECT'][$id]['OPTIMAL_TYPE_TRANSPORTATION'];
                $batch = $postDataForPlaning['MANUFACTURING_CHAIN_OBJECT'][$id]['OPTIMAL_BATCH_TRANSPORTATION'];
                $idEquipment = array_unique(array_column($postDataForPlaning['MANUFACTURING_CHAIN_OBJECT'][$id]['OPERATIONS'], 'UF_EQUIPMENT'));
                $equipments = $this->getEquipmentOperations($idEquipment);
                if (!is_int($date = array_pop($arrDateStart))) {
                    if ($forward) {
                        $dateStart = $arrDateStart[] = mktime(
                            $date['hour'],
                            $date['minute'],
                            $date['second'],
                            $date['month'],
                            $date['day'],
                            $date['year']
                        );
                    } else {
                        $dateEnd = mktime(
                            $date['hour'],
                            $date['minute'],
                            $date['second'],
                            $date['month'],
                            $date['day'],
                            $date['year']
                        );
                    }

                } else {
                    $arrDateStart[] = $date;
                }
                if ($forward) {
                    $this->allTypeManufacturingCalculationBlock(
                        $childrenObject,
                        $equipments,
                        $time,
                        date('d.m.Y H:i', array_sum($arrDateStart)),
                        $typeManufacturing,
                        $batch,
                        $forward
                    );
                } else {
                    $this->allTypeManufacturingCalculationBlock(
                        $childrenObject,
                        $equipments,
                        $time,
                        date('d.m.Y H:i', $dateEnd - array_sum($arrDateStart)),
                        $typeManufacturing,
                        $batch,
                        $forward
                    );
                }
                $arrDateStart[] = $time[$id]['TIME']['TIME'] * 60 * 60;
                $time[$id]['NAME'] = $childrenObject->nameObject;
                if ($dateEnd <= $calendar->getTimeStamp($time[$id]['TIME']['DATE_END'])) {
                    $dateStartForCalc = $calendar->getTimeStamp($time[$id]['TIME']['DATE_START']);
                    $dateEnd = $calendar->getTimeStamp($time[$id]['TIME']['DATE_END']);
                    $idEnd = $id;
                }
                if ($dateStart >= $calendar->getTimeStamp($time[$id]['TIME']['DATE_START']) || $dateStart == 0) {
                    $dateStart = $calendar->getTimeStamp($time[$id]['TIME']['DATE_START']);
                }

            }
            $postDataForPlaning['MANUFACTURING_MAIN_CHAIN_OBJECT'] = false;
            $arrDateStart = [$idEnd => $dateEnd - $dateStartForCalc];
        }

        if (!empty($object->treeObject)) {
            foreach ($object->treeObject as $id => $children) {
                if (is_int($id)) {
                    if (!empty($postDataForPlaning)) {
                        $typeManufacturing = $postDataForPlaning['MANUFACTURING_CHAIN_OBJECT'][$id]['OPTIMAL_TYPE_TRANSPORTATION'];
                        $batch = $postDataForPlaning['MANUFACTURING_CHAIN_OBJECT'][$id]['OPTIMAL_BATCH_TRANSPORTATION'];
                    }
                    $childrenObject = $object;
                    $childrenObject->idObject = $id;
                    $childrenObject->nameObject = $children['NAME'];
                    $childrenObject->operationsObject = $children['TECH_PROCESS']['OPERATIONS'];
                    $childrenObject->quantityObject = $children['QUANTITY'] ?? $object->treeObject['QUANTITY'];
                    $childrenObject->treeObject = $children;

                    if (empty($time[$id])) {
                        $idEquipment = array_unique(array_column($childrenObject->operationsObject, 'UF_EQUIPMENT'));
                        $equipments = $this->getEquipmentOperations($idEquipment);

                        if (!is_int($date = $arrDateStart[$idEnd])) {
                            $arrDateStart[$idEnd] = mktime(
                                $date['hour'],
                                $date['minute'],
                                $date['second'],
                                $date['month'],
                                $date['day'],
                                $date['year']
                            );
                        } else {
                            $arrDateStart[$idEnd] = $date;
                        }

                        $this->allTypeManufacturingCalculationBlock(
                            $object,
                            $equipments,
                            $time,
                            date('d.m.Y H:i', $dateEnd - array_sum($arrDateStart)),
                            $typeManufacturing,
                            $batch,
                            false
                        );
                    }
                    $arrDateStart[$id] = $time[$id]['TIME']['TIME'] * 60 * 60;
                    $time[$id]['NAME'] = $childrenObject->nameObject;

                    $this->fullCalculationCalendarTimeManufacturing($childrenObject, $time, $arrDateStart, $postDataForPlaning, $dateEnd, $dateStart, $idEnd, $forward);
                    $arrDateStart[$id] = 0;
                }
            }
        }
    }

    private function allTypeManufacturingCalculationBlock($object, $equipments, &$time, $timeStart, $typeManufacturing = false, $batch = 1, $forward = true)
    {
        if ($typeManufacturing) {
            if ($typeManufacturing == 'SEQUENTIALLY') {
                if ($forward) {
                    $time[$object->idObject]['TIME'] = $this->calculationSequentiallyTimeManufacturingForward(
                        $object,
                        $equipments,
                        $timeStart
                    );
                } else {
                    $time[$object->idObject]['TIME'] = $this->calculationSequentiallyTimeManufacturingInverse(
                        $object,
                        $equipments,
                        $timeStart
                    );
                }
            } elseif ($typeManufacturing == 'PARALLEL') {
                if ($forward) {
                    $time[$object->idObject]['TIME'] = $this->calculationParallelTimeManufacturingForward(
                        $object,
                        $equipments,
                        $timeStart,
                        $batch
                    );
                } else {
                    $time[$object->idObject]['TIME'] = $this->calculationParallelTimeManufacturingInverse(
                        $object,
                        $equipments,
                        $timeStart,
                        $batch
                    );
                }
            } else {
                if ($forward) {
                    $time[$object->idObject]['TIME'] = $this->calculationMixedTimeManufacturingForward(
                        $object,
                        $equipments,
                        $timeStart,
                        $batch
                    );
                } else {
                    $time[$object->idObject]['TIME'] = $this->calculationMixedTimeManufacturingInverse(
                        $object,
                        $equipments,
                        $timeStart,
                        $batch
                    );
                }
            }
        } else {
            $time[$object->idObject]['SEQUENTIALLY']['TIME'] = $this->calculationSequentiallyTimeManufacturingForward(
                $object,
                $equipments
            );
            $time[$object->idObject]['PARALLEL']['TIME'] = $this->calculationParallelTimeManufacturing(
                $object,
                $equipments
            );
            $optimalTypeTransportationPart = ($time[$object->idObject]['PARALLEL']['TIME']['TIME'] <= $time[$object->idObject]['SEQUENTIALLY']['TIME']['TIME']) ? 'PARALLEL' : 'SEQUENTIALLY';
            $time[$object->idObject]['MIXED']['TIME'] = $this->calculationMixedTimeManufacturing(
                $object,
                $equipments
            );
            $time[$object->idObject]['NAME_OBJECT'] = $object->nameObject;
            $time[$object->idObject]['QUANTITY_OBJECT'] = $object->quantityObject;

            $optimalTypeTransportationPart = ($optimalTypeTransportationPart <= $time[$object->idObject]['MIXED']['TIME']['TIME']) ? $optimalTypeTransportationPart : 'MIXED';

            $time[$object->idObject]['EQUIPMENTS'] = $this->pushTimeUseToArrayEquipment($object, $equipments);
            $time[$object->idObject]['OPERATIONS'] = $object->operationsObject;
            $time[$object->idObject]['OPTIMAL_TYPE_TRANSPORTATION'] = $optimalTypeTransportationPart;
            $time[$object->idObject]['MIN_TIME'] = $time[$object->idObject][$optimalTypeTransportationPart]['TIME']['TIME'];
            $time[$object->idObject]['OPTIMAL_BATCH_TRANSPORTATION'] = $time[$object->idObject][$optimalTypeTransportationPart]['TIME']['BATCH'];
            $time[$object->idObject]['START_TIME_FOR_GRAPH'] = $timeStart - $time[$object->idObject]['MIN_TIME'];
            $time[$object->idObject]['END_TIME_FOR_GRAPH'] = $timeStart;
        }
    }

    private function getWorkTimeCompany()
    {
        $workTime = new Calendar();
        return [
            'START' => $workTime->workTimeStart,
            'END' => $workTime->workTimeEnd
        ];
    }

    private function getEquipmentOperations($idEquipment = false)
    {
        $result = [];
        $filter = [];
        if ($idEquipment) {
            $filter = [
                'ID' => $idEquipment
            ];
        }
        $equipments = EquipmentTable::getList([
            'select' => ['ID', 'UF_COUNT', 'UF_NAME'],
            'filter' => $filter
        ])->fetchAll();
        foreach ($equipments as $equipment) {
            $result[$equipment['ID']] = $equipment;
        }

        return $result;
    }

    private function getTimeTransportation($idEquipments = [])
    {
        $timeData = new TimeTable();
        $time = $timeData->getList([
            'filter' => [
                'LOGIC' => 'OR',
                [
                    'UF_ID_EQUIPMENTS' => [
                        $idEquipments[0] . '.' . $idEquipments[1],
                    ],
                ],
                [
                    'UF_ID_EQUIPMENTS' => [
                        $idEquipments[1] . '.' . $idEquipments[0],
                    ]
                ]
            ]
        ])->fetchAll();
        return (int)$time[0]['UF_TIME'] ?? 0;
    }

    public function configureActions()
    {
        return [];
    }

    //вычисление календарного времени изготовления объекта по ajax
    public function ajaxCalculatePlanAction($post)
    {
        $object = new Factory($post['ID_OBJECT']);
        $result['OPERATIONS'] = $object->operationsObject;
        $idEquipment = array_unique(array_column($result['OPERATIONS'], 'UF_EQUIPMENT'));
        $result['EQUIPMENTS'] = $this->getEquipmentOperations();
        $batch = $post['BATCH_VALUE'] ?? 1;
        $recursive = ($object->treeObject[array_keys($object->treeObject)[0]]['HAVE_CHILD']) ? true : false;
        if ($recursive) {
            $timeManufacturingChainObject = [];
            $forwardCalculation = ($post['DATE_CALCULATE_TYPE'] == 'start') ? true : false;
            if ($forwardCalculation) {
                $dateStart = $this->addTimeToDate($post['DATE_VALUE'], 0);
                $timeStart[] = date_parse_from_format('d.m.Y H:i', $dateStart);
                $dateEnd = 0;
            } else {
                $dateStart = 0;
                $dateEnd = $this->addTimeToDate($post['DATE_VALUE'], 0);
                $timeStart[] = date_parse_from_format('d.m.Y H:i', $dateEnd);
            }
            $this->fullCalculationCalendarTimeManufacturing($object, $timeManufacturingChainObject, $timeStart, $post, $dateEnd, $dateStart, 0, $forwardCalculation);
            $result['MANUFACTURING_CHAIN_OBJECT'] = $timeManufacturingChainObject;
            $result["TIME"]["DATE_START"] = date_parse_from_format('d.m.Y H:i', date('d.m.Y H:i', $dateStart));
            $result["TIME"]["DATE_END"] = date_parse_from_format('d.m.Y H:i', date('d.m.Y H:i', $dateEnd));
            $result['TIME']['TIME'] = abs($dateStart - $dateEnd) / (60 * 60);
            $result['TIME']['TIME'] = (int)$result['TIME']['TIME'] . ' ч. ' . (int)(60 * ($result['TIME']['TIME'] - (int)$result['TIME']['TIME'])) . ' мин.';

        } else {
            $result['INFO_OBJECT'] = [
                'ID_OBJECT' => $object->idObject,
                'NAME_OBJECT' => $object->nameObject
            ];
            if ($post['MOVEMENT_TYPE'] == 'sequentially' || $object->quantityObject / $batch == 1) {
                if ($post['DATE_CALCULATE_TYPE'] == 'start') {
                    $result['TIME'] = $this->calculationSequentiallyTimeManufacturingForward($object, $result['EQUIPMENTS'], $post['DATE_VALUE']);
                    $result['TIME']['TIME'] = (int)$result['TIME']['TIME'] . 'ч. ' . (int)(60 * ($result['TIME']['TIME'] - (int)$result['TIME']['TIME'])) . ' мин.';
                    $result['TIME']['DATE_START'] = $this->addTimeToDate($post['DATE_VALUE'], 0, 'forward');
                }
                if ($post['DATE_CALCULATE_TYPE'] == 'end') {
                    $result['TIME'] = $this->calculationSequentiallyTimeManufacturingInverse($object, $result['EQUIPMENTS'], $post['DATE_VALUE']);
                    $result['TIME']['TIME'] = (int)$result['TIME']['TIME'] . 'ч. ' . (int)(60 * ($result['TIME']['TIME'] - (int)$result['TIME']['TIME'])) . ' мин.';
                    $result['TIME']['DATE_END'] = $this->addTimeToDate($post['DATE_VALUE'], 0, 'inverse');
                }
            } elseif ($post['MOVEMENT_TYPE'] == 'parallel') {
                if ($post['DATE_CALCULATE_TYPE'] == 'start') {
                    $result['TIME'] = $this->calculationParallelTimeManufacturingForward($object, $result['EQUIPMENTS'], $post['DATE_VALUE'], $batch);
                    $result['TIME']['TIME'] = (int)$result['TIME']['TIME'] . 'ч. ' . (int)(60 * ($result['TIME']['TIME'] - (int)$result['TIME']['TIME'])) . ' мин.';

                }
                if ($post['DATE_CALCULATE_TYPE'] == 'end') {
                    $result['TIME'] = $this->calculationParallelTimeManufacturingInverse($object, $result['EQUIPMENTS'], $post['DATE_VALUE'], $batch);
                    $result['TIME']['TIME'] = (int)$result['TIME']['TIME'] . 'ч. ' . (int)(60 * ($result['TIME']['TIME'] - (int)$result['TIME']['TIME'])) . ' мин.';
                }
            } else {
                if ($post['DATE_CALCULATE_TYPE'] == 'start') {
                    $result['TIME'] = $this->calculationMixedTimeManufacturingForward($object, $result['EQUIPMENTS'], $post['DATE_VALUE'], $batch);
                    $result['TIME']['TIME'] = (int)$result['TIME']['TIME'] . 'ч. ' . (int)(60 * ($result['TIME']['TIME'] - (int)$result['TIME']['TIME'])) . ' мин.';
                }
                if ($post['DATE_CALCULATE_TYPE'] == 'end') {
                    $result['TIME'] = $this->calculationMixedTimeManufacturingInverse($object, $result['EQUIPMENTS'], $post['DATE_VALUE'], $batch);
                    $result['TIME']['TIME'] = (int)$result['TIME']['TIME'] . 'ч. ' . (int)(60 * ($result['TIME']['TIME'] - (int)$result['TIME']['TIME'])) . ' мин.';
                }
            }
            $result['TIME']['DATE_END'] = date_parse_from_format('d.m.Y H:i', $result['TIME']['DATE_END']);
            $result['TIME']['DATE_START'] = date_parse_from_format('d.m.Y H:i', $result['TIME']['DATE_START']);
        }

        $result['PHP_STRONG'] = (int)(memory_get_peak_usage(true) / 1000000) . ' Mb';
        return $result;
    }

    //добавление выходного времени в прямом порядке
    public function addTimeHolidaysToPlanForwardTime($dateStart, $timeOperation, &$excludeHolidays = [])
    {
        $calendar = new Calendar();
        $planingOperationTime = $timeOperation;
        $dateStart = date_parse_from_format('d.m.Y H:i:s', $dateStart);
        $startDay = $dateStart['day'];
        $startMonth = $dateStart['month'];
        $startYear = $dateStart['year'];

        $startHour = (int)$dateStart['hour'] ?? (int)$calendar->workTimeStart;
        $startMinute = (int)$dateStart['minute'] ?? 0;

        $timestampStartDate = mktime($startHour, $startMinute, 0, $startMonth, $startDay, $startYear); //метка начала операции
        $timestampEndDate = $timestampStartDate + $timeOperation * 60 * 60; //метка окончания операции

        $startDate = $endDate = $timestampStartDate;
        for ($i = $timestampStartDate; $i <= $timestampEndDate; $i += (60 * 60)) {
            $endDate += $this->checkForNonworkingTimeAndOffsetEndTime($calendar, $i, $excludeHolidays, $timestampStartDate);
        }

        if ($startDate != $endDate) {
            $timeOperation = $timeOperation + ($endDate - $startDate) / (60 * 60);
            $startDate = date('d.m.Y H:i', $startDate);

            $planingOperationTime = $this->addTimeHolidaysToPlanForwardTime($startDate, $timeOperation, $excludeHolidays);
        }

        return $planingOperationTime;
    }

    //добавление выходного времени в обратном порядке
    public function addTimeHolidaysToPlanInverseTime($dateEnd, $timeOperation, &$excludeHolidays = [])
    {
        $calendar = new Calendar();
        $planingOperationTime = $timeOperation;

        $dateEnd = date_parse_from_format('d.m.Y H:i', $dateEnd);
        $endDay = $dateEnd['day'];
        $endMonth = $dateEnd['month'];
        $endYear = $dateEnd['year'];
        if (!$dateEnd['hour'] && !$dateEnd['minute']) {
            $endHour = (int)$calendar->workTimeEnd;
            $endMinute = 0;
        } else {
            $endHour = (int)$dateEnd['hour'];
            $endMinute = (int)$dateEnd['minute'];
        }

        $timestampEndDate = mktime($endHour, $endMinute, 0, $endMonth, $endDay, $endYear); //метка окончания операции
        $timestampStartDate = $timestampEndDate - $timeOperation * 60 * 60; //метка начала операции

        $startDate = $endDate = $timestampEndDate;
        for ($i = $timestampEndDate; $i >= $timestampStartDate; $i = $i - (60 * 60)) {
            $startDate -= $this->checkForNonworkingTimeAndOffsetEndTime($calendar, $i, $excludeHolidays, $timestampEndDate, true);
        }

        if ($startDate != $endDate) {
            $timeOperation = $timeOperation + abs($endDate - $startDate) / (60 * 60);
            $endDate = date('d.m.Y H:i', $endDate);
            $planingOperationTime = $this->addTimeHolidaysToPlanInverseTime($endDate, $timeOperation, $excludeHolidays);
        }

        return $planingOperationTime;
    }

    //проверка на ребачее вермя и смещение времени начала операции
    private function checkForNonworkingTimeAndOffsetEndTime($calendar, $time, &$excludeHolidays, $timestampStartDate, $inverseCalculation = false)
    {
        if ($calendar->isHoliday($time)) {
            if (!in_array(date('d.m.Y', $time), $excludeHolidays)) {
                $excludeHolidays[] = date('d.m.Y', $time);
                return 24 * 60 * 60;
            }
            return 0;
        }
        if (date('G', $time) > $calendar->workTimeEnd) {
            if (!in_array(date('d.m.Y H:i', $time), $excludeHolidays)) {
                $excludeHolidays[] = date('d.m.Y H:i', $time);
                return 60 * 60;
            }
            return 0;
        }
        if (date('G', $time) < $calendar->workTimeStart) {
            if (!in_array(date('d.m.Y H:i', $time), $excludeHolidays)) {
                $excludeHolidays[] = date('d.m.Y H:i', $time);
                return 60 * 60;
            }
            return 0;
        }
        if ($inverseCalculation) {
            $timeMark = $calendar->workTimeEnd;
        } else {
            $timeMark = $calendar->workTimeStart;
        }
        if ((int)date('G', $time) == $timeMark) {
            if (!in_array(date('d.m.Y H:i', $time), $excludeHolidays)) {
                $excludeHolidays[] = date('d.m.Y H:i', $time);
                if ($calendar->differenceTwoDays($timestampStartDate, $time) > 0) {
                    return 60 * 60;
                }
            }
            return 0;
        }
        return 0;
    }

    //добавление в массив оборудования суммарного времени использования
    private function pushTimeUseToArrayEquipment($object, $arrEquipment)
    {
        foreach ($object->operationsObject as $operation) {
            if (!$arrEquipment[$operation['UF_EQUIPMENT']]['TIME']) {
                $arrEquipment[$operation['UF_EQUIPMENT']]['TIME'] = 0;
            }
            $arrEquipment[$operation['UF_EQUIPMENT']]['TIME'] += $operation['UF_TIME'] * $object->quantityObject;
        }
        return $arrEquipment;
    }

    //добавление метки часов к дате
    private function addTimeToDate($date = 'date()', $time = 0, $type = 'forward')
    {
        $calendar = new Calendar();
        $date = date_parse_from_format('d.m.Y H:i', $date);
        if ($type == 'forward') {
            if (!$date['hour']) {
                $date['hour'] = $calendar->workTimeStart;
            }
            $date = mktime(
                    $date['hour'],
                    $date['minute'],
                    0,
                    $date['month'],
                    $date['day'],
                    $date['year']
                ) + $time * 60 * 60;
        }
        if ($type == 'inverse') {
            if (!$date['hour']) {
                $date['hour'] = $calendar->workTimeEnd;
            }
            $date = mktime(
                    $date['hour'],
                    $date['minute'],
                    0,
                    $date['month'],
                    $date['day'],
                    $date['year']
                ) - $time * 60 * 60;
        }
        $date = date('d.m.Y H:i', $date);

        return $date;
    }

    //добавление выходного времени ко времени операций
    private function addDataForCalendarPlaningToPersonalGroupByOperation(&$operation, $dataOperation, $date, $timeLengthOperation, $inverseCalculation = false)
    {
        $operation[$dataOperation['ID']]['NAME'] = $dataOperation['UF_NAME'];
        $operation[$dataOperation['ID']]['TIME_OPERATION'] = $timeLengthOperation;
        $operation[$dataOperation['ID']]['ID_EQUIPMENT'] = $dataOperation['UF_EQUIPMENT'];
        if (!$inverseCalculation) {
            $operation[$dataOperation['ID']]['DATE_START'] = $this->addTimeToDate($date, 0, 'forward');
            $operation[$dataOperation['ID']]['DATE_END'] = $this->addTimeToDate($date, $timeLengthOperation, 'forward');
        } else {
            $operation[$dataOperation['ID']]['DATE_END'] = $this->addTimeToDate($date, 0, 'inverse');
            $operation[$dataOperation['ID']]['DATE_START'] = $this->addTimeToDate($date, $timeLengthOperation, 'inverse');
        }
        $operation[$dataOperation['ID']]['GRAPH_DATE_END'] = date_parse_from_format('d.m.Y H:i', $operation[$dataOperation['ID']]['DATE_END']);
        $operation[$dataOperation['ID']]['GRAPH_DATE_START'] = date_parse_from_format('d.m.Y H:i', $operation[$dataOperation['ID']]['DATE_START']);
    }

    private function calculationMixedTimeManufacturingInverse($object, $equipments, $dateEndForPlaning, $batch = 1)
    {
        $dateEndForResultReturn = $this->addTimeToDate($dateEndForPlaning, 0);
        $calendarOperationPlan = [];
        $objectOperations = array_reverse($object->operationsObject);
        $countIterationsCalculation = (int)($object->quantityObject / $batch);
        $previousTimeOperation = false;
        $nextBatch = false;
        $dateEnd = $dateStart = $dateEndForResultReturn;
        $arIdOperationsEquipment = array_column($objectOperations, 'UF_EQUIPMENT');

        foreach ($objectOperations as $key => $operation) {
            $time = 0;
            $excludeHolidays = [];

            if ($arIdOperationsEquipment[$key + 1]) {
                $timeTransportation = $this->getTimeTransportation([$arIdOperationsEquipment[$key], $arIdOperationsEquipment[$key + 1]]);
            } else {
                $timeTransportation = $this->getTimeTransportation([$arIdOperationsEquipment[$key], 0]);
            }

            $quantityEquipments = $equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] ? (int)$equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] : 1;
            $timeOperation = ((int)$operation['UF_TIME'] / $quantityEquipments) * $batch;
            if ($nextBatch && $previousTimeOperation) {
                if ($previousTimeOperation > $timeOperation + $timeTransportation) {
                    if ($countIterationsCalculation > 1) {
                        $dateEnd = $this->addTimeToDate(
                            $dateStart,
                            $this->addTimeHolidaysToPlanForwardTime(
                                $dateStart,
                                ($timeTransportation + $timeOperation) * ($countIterationsCalculation - 1)
                            )
                        );
                    } else {
                        //такого быть не дложно. Если одинкаовое количество и партийность то считается обычный параллельный цикл
                        $dateEnd = $this->addTimeToDate(
                            $dateStart,
                            $this->addTimeHolidaysToPlanForwardTime(
                                $dateStart,
                                ($timeTransportation + $timeOperation) * $countIterationsCalculation
                            )
                        );
                    }
                } else {
                    $dateEnd = $this->addTimeToDate(
                        $dateEnd,
                        $this->addTimeHolidaysToPlanInverseTime(
                            $dateEnd,
                            $previousTimeOperation
                        ),
                        'inverse'
                    );
                }
            } else {
                $dateEnd = $this->addTimeToDate($dateEndForPlaning, $time);
            }
            $calendarTime = $this->addTimeHolidaysToPlanInverseTime($dateEnd, ($timeTransportation + $timeOperation) * $countIterationsCalculation, $excludeHolidays);
            $this->addDataForCalendarPlaningToPersonalGroupByOperation($calendarOperationPlan[1], $operation, $dateEnd, $calendarTime, true);
            $dateStart = $this->addTimeToDate($dateEnd, $calendarTime, 'inverse');
            $previousTimeOperation = $timeOperation + $timeTransportation;
            $nextBatch = true;
        }
        $calendar = new Calendar();
        $time = abs($calendar->getTimeStamp($dateStart) - $calendar->getTimeStamp($dateEndForResultReturn)) / (60 * 60);

        return [
            'BATCH' => $batch,
            'TIME' => $time,
            'DATE_START' => $dateStart,
            'DATE_END' => $dateEndForResultReturn,
            'PLAN_OPERATIONS' => $calendarOperationPlan,
        ];
    }

    private function calculationMixedTimeManufacturingForward($object, $equipments, $dateStartForPlaning, $batch = 1)
    {
        $dateStartForResultReturn = $this->addTimeToDate($dateStartForPlaning, 0);
        $calendarOperationPlan = [];
        $objectOperations = $object->operationsObject;
        $countIterationsCalculation = (int)($object->quantityObject / $batch);
        $previousTimeOperation = false;
        $nextBatch = false;
        $dateEnd = $dateStart = $dateStartForResultReturn;

        $idPreviousEquipment = 0;
        foreach ($objectOperations as $key => $operation) {
            $time = 0;
            $excludeHolidays = [];
            $timeTransportation = $this->getTimeTransportation([$idPreviousEquipment, $equipments[$operation['UF_EQUIPMENT']]['ID']]);

            $idPreviousEquipment = $equipments[$operation['UF_EQUIPMENT']]['ID'];
            $quantityEquipments = $equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] ? (int)$equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] : 1;
            $timeOperation = ((int)$operation['UF_TIME'] / $quantityEquipments) * $batch;
            if ($nextBatch && $previousTimeOperation) {
                if ($previousTimeOperation > $timeOperation + $timeTransportation) {
                    if ($countIterationsCalculation > 1) {

                        $dateStart = $this->addTimeToDate(
                            $dateEnd,
                            $this->addTimeHolidaysToPlanInverseTime(
                                $dateEnd,
                                ($timeTransportation + $timeOperation) * ($countIterationsCalculation - 1)
                            ),
                            'inverse'
                        );
                    } else {
                        //такого быть не дложно. Если одинкаовое количество и партийность то считается обычный параллельный цикл
                        $dateStart = $this->addTimeToDate(
                            $dateEnd,
                            $this->addTimeHolidaysToPlanInverseTime(
                                $dateEnd,
                                ($timeTransportation + $timeOperation) * $countIterationsCalculation
                            ),
                            'inverse'
                        );
                    }
                } else {
                    $dateStart = $this->addTimeToDate(
                        $dateStart,
                        $this->addTimeHolidaysToPlanForwardTime(
                            $dateStart,
                            $previousTimeOperation
                        )
                    );
                }
            } else {
                $dateStart = $this->addTimeToDate($dateStartForPlaning, $time);
            }
            $calendarTime = $this->addTimeHolidaysToPlanForwardTime($dateStart, ($timeTransportation + $timeOperation) * $countIterationsCalculation, $excludeHolidays);
            $this->addDataForCalendarPlaningToPersonalGroupByOperation($calendarOperationPlan[1], $operation, $dateStart, $calendarTime);
            $dateEnd = $this->addTimeToDate($dateStart, $calendarTime);
            $previousTimeOperation = $timeOperation + $timeTransportation;
            $nextBatch = true;
        }
        $calendar = new Calendar();
        $time = abs($calendar->getTimeStamp($dateEnd) - $calendar->getTimeStamp($dateStartForResultReturn)) / (60 * 60);

        return [
            'BATCH' => $batch,
            'TIME' => $time,
            'DATE_START' => $dateStartForResultReturn,
            'DATE_END' => $dateEnd,
            'PLAN_OPERATIONS' => $calendarOperationPlan,
        ];
    }

    //Параллельное движение партий (обратное вычисление)
    private function calculationParallelTimeManufacturingInverse($object, $equipments, $dateStartForPlaning, $batch = 1)
    {
        $dateStartForResultReturn = $this->addTimeToDate($dateStartForPlaning, 0);
        $calendarOperationPlan = [];
        $maxLengthTimeOfOperation = max(array_column($object->operationsObject, 'UF_TIME'));
        $dateStartCalculationNextButch = false;
        $countIterationsCalculation = (int)($object->quantityObject / $batch);
        $objectOperations = array_reverse($object->operationsObject);
        $nextBatch = false;

        $arIdOperationsEquipment = array_column($objectOperations, 'UF_EQUIPMENT');
        for ($i = $countIterationsCalculation; $i >= 1; $i--) {
            $inverseArrayOperation = [];
            $time = 0;
            foreach ($objectOperations as $key => $operation) {
                $excludeHolidays = [];

                if ($arIdOperationsEquipment[$key + 1]) {
                    $timeTransportation = $this->getTimeTransportation([$arIdOperationsEquipment[$key], $arIdOperationsEquipment[$key + 1]]);
                } else {
                    $timeTransportation = $this->getTimeTransportation([$arIdOperationsEquipment[$key], 0]);
                }

                $quantityEquipments = $equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] ? (int)$equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] : 1;
                $quantityEquipments = $this->getQuantityEquipmentForOperation($quantityEquipments, $batch);
                $timeOperation = (((int)$timeTransportation + (int)$operation['UF_TIME']) / $quantityEquipments) * $batch;
                array_push(
                    $inverseArrayOperation,
                    [
                        'UF_TIME' => $timeOperation,
                        'UF_EQUIPMENT' => $operation['UF_EQUIPMENT'],
                        'ID' => $operation['ID'],
                        'UF_NAME' => $operation['UF_NAME']
                    ]
                );

                if ($dateStartCalculationNextButch && $nextBatch) {
                    $dateStart = $dateStartCalculationNextButch;
                    $dateStartForPlaning = $dateStartCalculationNextButch;
                    $dateStartCalculationNextButch = false;
                    $nextBatch = false;
                } else {
                    $dateStart = $this->addTimeToDate($dateStartForPlaning, $time, 'inverse');
                }
                $calendarTime = $this->addTimeHolidaysToPlanInverseTime($dateStart, $timeOperation, $excludeHolidays);
                $this->addDataForCalendarPlaningToPersonalGroupByOperation($calendarOperationPlan[$i], $operation, $dateStart, $calendarTime, true);
                $time += $calendarTime;
                if (!$dateStartCalculationNextButch && $maxLengthTimeOfOperation == $operation['UF_TIME']) {
                    $dateStartCalculationNextButch = $this->addTimeToDate($dateStartForPlaning, $time, 'inverse');
                    $timeMaxOperationNextButch = $this->addTimeHolidaysToPlanInverseTime($dateStartCalculationNextButch, $timeOperation);
                    $dateStartCalculationNextButch = $this->addTimeToDate($dateStartCalculationNextButch, $timeMaxOperationNextButch, 'inverse');
                    $inverseObject = $object;
                    $inverseObject->operationsObject = $inverseArrayOperation;
                    $inverseObject->quantityObject = 1; //уже передаем учет партии во времени операций
                    $dateStartCalculationNextButch = $this->calculationSequentiallyTimeManufacturingForward($inverseObject, $equipments, $dateStartCalculationNextButch, false)['DATE_END'];
                }
            }
            $nextBatch = true;
        }
        $calendar = new Calendar();
        $dateEnd = $this->addTimeToDate($dateStartForPlaning, $time, 'inverse');
        $dateStart = $calendar->getTimeStamp($dateStartForResultReturn);
        $time = abs($calendar->getTimeStamp($dateEnd) - $dateStart) / (60 * 60);

        return [
            'BATCH' => $batch,
            'TIME' => $time,
            'DATE_START' => $dateEnd,
            'DATE_END' => $dateStartForResultReturn,
            'PLAN_OPERATIONS' => $calendarOperationPlan,
        ];
    }

    //Параллельное движение партий (прямое вычисление)
    private function calculationParallelTimeManufacturingForward($object, $equipments, $dateStartForPlaning, $batch = 1)
    {
        $dateStartForResultReturn = $this->addTimeToDate($dateStartForPlaning, 0);
        $calendarOperationPlan = [];
        $maxLengthTimeOfOperation = max(array_column($object->operationsObject, 'UF_TIME'));
        $dateStartCalculationNextButch = false;
        $countIterationsCalculation = (int)($object->quantityObject / $batch);
        $objectOperations = $object->operationsObject;
        $nextBatch = false;

        for ($i = 1; $i <= $countIterationsCalculation; $i++) {
            $inverseArrayOperation = [];
            $time = 0;
            $idPreviousEquipment = 0;
            foreach ($objectOperations as $key => $operation) {
                $excludeHolidays = [];

                $timeTransportation = $this->getTimeTransportation([$idPreviousEquipment, $operation['UF_EQUIPMENT']]);

                $idPreviousEquipment = $equipments[$operation['UF_EQUIPMENT']]['ID'];
                $quantityEquipments = $equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] ? (int)$equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] : 1;
                $quantityEquipments = $this->getQuantityEquipmentForOperation($quantityEquipments, $batch);
                $timeOperation = (($timeTransportation + $operation['UF_TIME']) / $quantityEquipments) * $batch;
                array_push(
                    $inverseArrayOperation,
                    [
                        'UF_TIME' => $timeOperation,
                        'UF_EQUIPMENT' => $operation['UF_EQUIPMENT'],
                        'ID' => $operation['ID'],
                        'UF_NAME' => $operation['UF_NAME']
                    ]
                );

                if ($dateStartCalculationNextButch && $nextBatch) {
                    $dateStart = $dateStartCalculationNextButch;
                    $dateStartForPlaning = $dateStartCalculationNextButch;
                    $dateStartCalculationNextButch = false;
                    $nextBatch = false;
                } else {
                    $dateStart = $this->addTimeToDate($dateStartForPlaning, $time);
                }
                $calendarTime = $this->addTimeHolidaysToPlanForwardTime($dateStart, $timeOperation, $excludeHolidays);
                $this->addDataForCalendarPlaningToPersonalGroupByOperation($calendarOperationPlan[$i], $operation, $dateStart, $calendarTime);
                $time += $calendarTime;
                if (!$dateStartCalculationNextButch && $maxLengthTimeOfOperation == $operation['UF_TIME']) {
                    $dateStartCalculationNextButch = $this->addTimeToDate($dateStartForPlaning, $time);
                    $timeMaxOperationNextButch = $this->addTimeHolidaysToPlanForwardTime($dateStartCalculationNextButch, $timeOperation);
                    $dateStartCalculationNextButch = $this->addTimeToDate($dateStartCalculationNextButch, $timeMaxOperationNextButch);
                    $inverseObject = $object;
                    $inverseObject->operationsObject = $inverseArrayOperation;
                    $inverseObject->quantityObject = 1; //уже передаем учет партии во времени операций
                    $dateStartCalculationNextButch = $this->calculationSequentiallyTimeManufacturingInverse($inverseObject, $equipments, $dateStartCalculationNextButch, false)['DATE_START'];
                }
            }
            $nextBatch = true;
        }
        $calendar = new Calendar();
        $dateEnd = $this->addTimeToDate($dateStartForPlaning, $time);
        $dateStart = $calendar->getTimeStamp($dateStartForResultReturn);
        $time = ($calendar->getTimeStamp($dateEnd) - $dateStart) / (60 * 60);

        return [
            'BATCH' => $batch,
            'TIME' => $time,
            'DATE_END' => $dateEnd,
            'DATE_START' => $dateStartForResultReturn,
            'PLAN_OPERATIONS' => $calendarOperationPlan,
        ];
    }

    //Последовательное движение партий (обратное вычисление)
    private function calculationSequentiallyTimeManufacturingInverse($object, $equipments, $dateEndForPlaning = false, $useTransportTime = true, $batch = false)
    {
        $calendarOperationPlan = [];
        $time = 0;
        $operations = array_reverse($object->operationsObject);
        $arIdOperationsEquipment = array_column($operations, 'UF_EQUIPMENT');

        foreach ($operations as $key => $operation) {
            if ($useTransportTime) {
                if ($arIdOperationsEquipment[$key + 1]) {
                    $timeTransportation = $this->getTimeTransportation([$arIdOperationsEquipment[$key], $arIdOperationsEquipment[$key + 1]]);
                } else {
                    $timeTransportation = $this->getTimeTransportation([$arIdOperationsEquipment[$key], 0]);
                }
            } else {
                $timeTransportation = 0;
            }
            $quantityEquipments = $equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] ? (int)$equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] : 1;
            if ($batch) {
                $quantityObject = $batch;
            } else {
                $quantityObject = $object->quantityObject;
            }
            $quantityEquipments = $this->getQuantityEquipmentForOperation($quantityEquipments, $quantityObject);

            $timeOperation = (int)$operation['UF_TIME'] / $quantityEquipments;
            if ($dateEndForPlaning) {
                $dateEnd = $dateEndForPlaning;
                if ($time > 0) {
                    $dateEnd = $this->addTimeToDate($dateEndForPlaning, $time, 'inverse');
                }
                $excludeHolidays = [];
                $calendarTime = $this->addTimeHolidaysToPlanInverseTime($dateEnd, $timeTransportation + $timeOperation * $object->quantityObject, $excludeHolidays);
                $this->addDataForCalendarPlaningToPersonalGroupByOperation($calendarOperationPlan[1], $operation, $dateEnd, $calendarTime, true);
                $time += $calendarTime;
            } else {
                $time += $timeTransportation + $timeOperation * $object->quantityObject;
            }
        }
        $dateStart = $this->addTimeToDate($dateEndForPlaning, $time, 'inverse');
        return ['BATCH' => $object->quantityObject, 'TIME' => $time, 'DATE_START' => $dateStart, 'PLAN_OPERATIONS' => $calendarOperationPlan, 'EXCLUDE' => $excludeHolidays];
    }

    //Последовательное движение партий (прямое вычисление)
    private function calculationSequentiallyTimeManufacturingForward($object, $equipments, $dateStartForPlaning = false, $useTransportTime = true, $batch = false, $recursive = false)
    {
        $time = 0;
        $calendarOperationPlan = [];
        $idPreviousEquipment = 0;
        if (!$recursive) {
            foreach ($object->operationsObject as $key => $operation) {
                if ($useTransportTime) {
                    $timeTransportation = $this->getTimeTransportation([$idPreviousEquipment, $equipments[$operation['UF_EQUIPMENT']]['ID']]);
                    $idPreviousEquipment = $equipments[$operation['UF_EQUIPMENT']]['ID'];
                } else {
                    $timeTransportation = 0;
                }
                $quantityEquipments = $equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] ? (int)$equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] : 1;
                if ($batch) {
                    $quantityObject = $batch;
                } else {
                    $quantityObject = $object->quantityObject;
                }
                $quantityEquipments = $this->getQuantityEquipmentForOperation($quantityEquipments, $quantityObject);
                $timeOperation = $operation['UF_TIME'] / $quantityEquipments;
                if ($dateStartForPlaning) {
                    $dateStart = $this->addTimeToDate($dateStartForPlaning, 0);
                    if ($time > 0) {
                        $dateStart = $this->addTimeToDate($dateStartForPlaning, $time);
                    }
                    $excludeHolidays = [];
                    $calendarTime = $this->addTimeHolidaysToPlanForwardTime($dateStart, $timeTransportation + $timeOperation * $object->quantityObject, $excludeHolidays);
                    $this->addDataForCalendarPlaningToPersonalGroupByOperation($calendarOperationPlan[1], $operation, $dateStart, $calendarTime);
                    $time += $calendarTime;
                } else {
                    $this->addDataForCalendarPlaningToPersonalGroupByOperation($calendarOperationPlan[1], $operation, $this->addTimeToDate('', $time), $timeTransportation + $timeOperation * $object->quantityObject);
                    $time += $timeTransportation + $timeOperation * $object->quantityObject;
                }
            }
            $dateEnd = $this->addTimeToDate($dateStartForPlaning, $time);
            if (!empty($object->treeObject)) {
//                $recursive = true;
            }
        }

        if (!empty($object->treeObject) && $recursive) {
            foreach ($object->treeObject as $id => $children) {
                if (is_int($id)) {
                    $idEquipment = array_unique(array_column($children['TECH_PROCESS']['OPERATIONS'], 'UF_EQUIPMENT'));
                    $equipments = $this->getEquipmentOperations($idEquipment);
                    $childrenObject = $object;
                    $childrenObject->idObject = $id;
                    $childrenObject->nameObject = $children['NAME'];
                    $childrenObject->operationsObject = $children['TECH_PROCESS']['OPERATIONS'];
                    $childrenObject->quantityObject = $children['QUANTITY'] ?? $object->treeObject['QUANTITY'];
                    $childrenObject->treeObject = $children;

                    $time += $this->calculationSequentiallyTimeManufacturingForward(
                        $childrenObject,
                        $equipments,
                        false,
                        true,
                        false,
                        false
                    )['TIME'];
                }
            }
        }

        return [
            'BATCH' => $object->quantityObject,
            'TIME' => $time,
            'DATE_END' => $dateEnd,
            'PLAN_OPERATIONS' => $calendarOperationPlan,
            'EXCLUDE' => $excludeHolidays
        ];
    }

    //Смешанное движение партий
    private function calculationMixedTimeManufacturing($object, $equipments, $batches = false, $arTimes = [])
    {
        $firstBatch = 1;
        $secondBatch = $object->quantityObject;

        for ($j = $firstBatch - 1; $j <= $secondBatch + 1; $j++) {

            if (is_array($arTimes[$j]) || $j <= 0 || !is_int((int)$object->quantityObject / (int)$j)) {
                continue;
            }
            $arTimes[$j] = 0;
            $SequentiallyTime = $this->calculationSequentiallyTimeManufacturingForward($object, $equipments, false, false, $j)['TIME'];
            $idPreviousEquipment = 0;
            foreach ($object->operationsObject as $operation) {
                $timeTransportation = $this->getTimeTransportation([$idPreviousEquipment, $equipments[$operation['UF_EQUIPMENT']]['ID']]);
                $idPreviousEquipment = $equipments[$operation['UF_EQUIPMENT']]['ID'];
                $SequentiallyTime += $timeTransportation * (int)$object->quantityObject / (int)$j;
            }
            $idPreviousEquipment = 0;
            for ($i = 0; $i < count($object->operationsObject) - 1; $i++) {
                $timeTransportationCurrent = $this->getTimeTransportation([
                    $idPreviousEquipment,
                    $equipments[$object->operationsObject[$i]['UF_EQUIPMENT']]['ID']
                ]);
                $timeTransportationNext = $this->getTimeTransportation([
                    $equipments[$object->operationsObject[$i]['UF_EQUIPMENT']]['ID'],
                    $equipments[$object->operationsObject[$i + 1]['UF_EQUIPMENT']]['ID']
                ]);
                $idPreviousEquipment = $equipments[$object->operationsObject[$i]['UF_EQUIPMENT']]['ID'];
                $quantityEquipmentsCurrent = (int)$equipments[(int)$object->operationsObject[$i]['UF_EQUIPMENT']]['UF_COUNT'] ?? 1;
                $quantityEquipmentsCurrent = $this->getQuantityEquipmentForOperation($quantityEquipmentsCurrent, $j);
                $quantityEquipmentsNext = (int)$equipments[(int)$object->operationsObject[$i + 1]['UF_EQUIPMENT']]['UF_COUNT'] ?? 1;
                $quantityEquipmentsNext = $this->getQuantityEquipmentForOperation($quantityEquipmentsNext, $j);
                if (
                    $timeTransportationCurrent + $object->operationsObject[$i]['UF_TIME'] / $quantityEquipmentsCurrent
                    <
                    $timeTransportationNext + $object->operationsObject[$i + 1]['UF_TIME'] / $quantityEquipmentsNext
                ) {
                    $timeTransportation = $timeTransportationCurrent;
                    $minTimeOperation = $timeTransportation / $j + $object->operationsObject[$i]['UF_TIME'] / $quantityEquipmentsCurrent;
                } else {
                    $timeTransportation = $timeTransportationNext;
                    $minTimeOperation = $timeTransportation / $j + $object->operationsObject[$i + 1]['UF_TIME'] / $quantityEquipmentsNext;
                }
                $arTimes[$j] += $minTimeOperation;
            }
            $arTimes[$j] = $SequentiallyTime - ((int)$object->quantityObject - $j) * $arTimes[$j];
        }
        return ['BATCH' => array_search(min($arTimes), $arTimes), 'TIME' => min($arTimes)];
    }

    //Параллельное движение партий
    private function calculationParallelTimeManufacturing($object, $equipments, $batches = false, $arTimes = [])
    {

        $firstBatch = 1;
        $secondBatch = $object->quantityObject;
        for ($j = $firstBatch - 1; $j <= $secondBatch + 1; $j++) {
            if (is_array($arTimes[$j]) || $j <= 0 || !is_int((int)$object->quantityObject / (int)$j)) {
                continue;
            }
            $arTimes[$j] = 0;
            $idPreviousEquipment = 0;
            foreach ($object->operationsObject as $key => $operation) {

                $timeTransportation = $this->getTimeTransportation([$idPreviousEquipment, $operation['UF_EQUIPMENT']]);

                $idPreviousEquipment = $equipments[$operation['UF_EQUIPMENT']]['ID'];
                $quantityEquipments = (int)$equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] ?? 1;
                $quantityEquipments = $this->getQuantityEquipmentForOperation($quantityEquipments, $j);


                $arTimes[$j] += $timeTransportation + $j * ($operation['UF_TIME'] / $quantityEquipments);
            }
            $maxOperationTime = $this->getMaxOperationTime($object, $equipments, $j);

            $arTimes[$j] += ($object->quantityObject - $j) * $maxOperationTime / $j;
        }

        return ['BATCH' => array_search(min($arTimes), $arTimes), 'TIME' => min($arTimes)];
    }

    //максимальная по времени операция
    private function getMaxOperationTime($object, $equipments, $batch = 1)
    {
        $maxTime = 0;
        $idPreviousEquipment = 0;
        foreach ($object->operationsObject as $operation) {
            $timeTransportation = $this->getTimeTransportation([$idPreviousEquipment, $equipments[$operation['UF_EQUIPMENT']]['ID']]);
            $idPreviousEquipment = $equipments[$operation['UF_EQUIPMENT']]['ID'];
            $quantityEquipments = (int)$equipments[$operation['UF_EQUIPMENT']]['UF_COUNT'] ?? 1;
            $quantityEquipments = $this->getQuantityEquipmentForOperation($quantityEquipments, $batch);
            $time = $timeTransportation + $batch * ($operation['UF_TIME'] / $quantityEquipments);
            $maxTime = ($time > $maxTime) ? $time : $maxTime;
        }
        return $maxTime;
    }

    //Интервал расчетной партийности
    private function getArrayBatches($firstBatch, $secondBatch, $firstTime, $secondTime)
    {
        if ($firstTime > $secondTime) {
            $secondBatch += round(abs($secondBatch - $firstBatch) / 2);
            $step = round(abs($firstBatch - $secondBatch) / 4);
            $secondBatch -= $step;
            $firstBatch = $secondBatch - 2 * $step;
        } else {
            $firstBatch -= round(abs($secondBatch - $firstBatch) / 2);
            $step = round(abs($firstBatch - $secondBatch) / 4);
            $firstBatch = $firstBatch + $step;
            $secondBatch = $firstBatch + 2 * $step;
        }
        if ($firstBatch <= 0) $firstBatch = 1;
        return [$firstBatch, $secondBatch];
    }

    //получение количества используемого оборудования
    private function getQuantityEquipmentForOperation($countExistEquipment = 1, $batch = 1)
    {
        $quantity = $countExistEquipment;
        if ($countExistEquipment >= $batch) {
            $quantity = $batch;
        } else {
            for ($i = $countExistEquipment; $i >= 1; $i--) {
                if ((int)($batch / $i) == $batch / $i) {
                    $quantity = $i;
                    break;
                }
            }
        }
        return $quantity;
    }
}
