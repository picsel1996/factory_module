$(function () {
    console.log(manufacturing_main_chain_object);
});

function calculatePlaning() {
    var data = getParamsForCalculate();
    callMethodByAjax(
        'bessonov:production.manufacturing.time',
        'ajaxCalculatePlan',
        data
    ).then(
        function (result) {
            console.log(result.data);
            $('div#result-calculate').empty();
            showHeadResultCalculationBlock();
            showResultCalculation(result.data);
            showPlanOfOperations(result.data);
            drawManufacturingGraphic(result.data);
        },
        function (error) {
            console.log(error);
        }
    );
};

function drawManufacturingGraphic(result) {
    google.charts.load("current", {packages: ["timeline"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var container = document.getElementById('graph-manufacturing-calendar');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn({type: 'string', id: 'Duration'});
        dataTable.addColumn({type: 'string', id: 'Name'});
        dataTable.addColumn({type: 'date', id: 'Start'});
        dataTable.addColumn({type: 'date', id: 'End'});
        var arrDateData = [];
        if (result['MANUFACTURING_CHAIN_OBJECT']) {
            $.each(result['MANUFACTURING_CHAIN_OBJECT'], function (id, object) {
                $.each(object['TIME']['PLAN_OPERATIONS'], function (idObject, batch) {
                    $.each(batch, function (idOperation, operation) {
                        arrDateData.push([
                            operation['NAME'],
                            object['NAME'],
                            getDateByArray(operation["GRAPH_DATE_START"]),
                            getDateByArray(operation["GRAPH_DATE_END"])
                        ]);
                    });

                });
            });
        } else {
            $.each(result['TIME']['PLAN_OPERATIONS'], function (id, object) {
                $.each(object, function (idObject, operation) {
                    arrDateData.push([
                        operation['NAME'],
                        result['INFO_OBJECT']['NAME_OBJECT'],
                        getDateByArray(operation["GRAPH_DATE_START"]),
                        getDateByArray(operation["GRAPH_DATE_END"])
                    ]);
                });
            });
        }

        dataTable.addRows(arrDateData);
        var options = {
            timeline: {
                colorByRowLabel: false,
                groupByRowLabel: true
            }
        };
        chart.draw(dataTable, options);
    }
}

function showHeadResultCalculationBlock() {
    var dataContainer = $('div#result-calculate');
    var contentContainer = '<div class="row" id="header-calculation">\n' +
        '        <hr>\n' +
        '        <div class="col-md-12">\n' +
        '            <h4>Результат расчета</h4>\n' +
        '        </div>\n' +
        '    </div>';
    dataContainer.append(contentContainer);
}

function getDateByArray(arrDate) {
    return new Date(arrDate['year'], arrDate['month']-1, arrDate['day'], arrDate['hour'], arrDate['minute'], arrDate['second']);
}

function showResultCalculation(result) {
    var dataContainer = $('div#result-calculate');
    var contentContainer = '<div class="row">' +
        '<div class="col-xs-12" >\n' +
        '            <div class="row">\n' +
        '                <div class="col-md-3">\n' +
        '                    Дата начала изготовления\n' +
        '                </div>\n' +
        '                <div class="col-md-6">\n' +
        '                    ' + getDateByArray(result["TIME"]["DATE_START"]) + '\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="row">\n' +
        '                <div class="col-md-3">\n' +
        '                    Дата окончания изготовления\n' +
        '                </div>\n' +
        '                <div class="col-md-6">\n' +
        '                    ' + getDateByArray(result["TIME"]["DATE_END"]) + '\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="row">\n' +
        '                <div class="col-md-3">\n' +
        '                    Длительность операционного цикла\n' +
        '                </div>\n' +
        '                <div class="col-md-6">\n' +
        '                    ' + result["TIME"]["TIME"] + '\n' +
        '                </div>\n' +
        '            </div>\n' +
        '    </div>' +
        '</div>';
    dataContainer.append(contentContainer);
}

function showPlanOfOperations(result) {
    var dataContainer = $('div#result-calculate');
    var contentContainer = '<div class="row">\n' +
        '        <hr>\n' +
        '        <div class="col-md-12">\n' +
        '            <h4>План работ </h4>\n' +
        '        </div>\n' +
        '    </div>';
    contentContainer += '<br><div class="row">' +
        '<div class="col-xs-12" >\n' +
        '<div class="row border-bottom">\n' +
        '                <div class="col-md-6">\n' +
        '                    Операция\n' +
        '                </div>\n' +
        '                <div class="col-md-3 border-left">\n' +
        '                    Оборудование\n' +
        '                </div>\n' +
        '                <div class="col-md-3 border-left">\n' +
        '                    Даты начала-окончания\n' +
        '                </div>\n' +
        '</div><br>';
    var quantityOperation = 0;

    if (result['MANUFACTURING_CHAIN_OBJECT']) {
        $.each(result['MANUFACTURING_CHAIN_OBJECT'], function (id, object) {
            contentContainer += '' +
                '            <div class="row">\n' +
                '                <div class="col-md-3 border-bottom  border-left">\n' +
                '                    ' + object['NAME'] + '\n' +
                '                </div>\n' +
                '            </div>' +
                '\n';
            $.each(object['TIME']['PLAN_OPERATIONS'], function (idObject, batch) {
                $.each(batch, function (idOperation, operation) {
                    quantityOperation++;
                    contentContainer += '' +
                        '            <div class="row">\n' +
                        '                <div class="col-md-6">\n' +
                        '                    ' + operation['NAME'] + '\n' +
                        '                </div>\n' +
                        '                <div class="col-md-3 border-bottom  border-left">\n' +
                        '                    ' + result['EQUIPMENTS'][operation['ID_EQUIPMENT']]['UF_NAME'] + '\n' +
                        '                </div>\n' +
                        '                <div class="col-md-3 border-bottom  border-left">\n' +
                        '                    ' + operation["DATE_START"] + ' - ' + operation["DATE_END"] + '\n' +
                        '                </div>\n' +
                        '            </div>' +
                        '\n';
                });

                contentContainer += '<hr>';
            });
        });
    } else {
        $.each(result['TIME']['PLAN_OPERATIONS'], function (id, object) {
            $.each(object, function (idObject, operation) {
                quantityOperation++;

                contentContainer += '' +
                    '            <div class="row">\n' +
                    '                <div class="col-md-6">\n' +
                    '                    ' + operation['NAME'] + '\n' +
                    '                </div>\n' +
                    '                <div class="col-md-3 border-bottom  border-left">\n' +
                    '                    ' + result['EQUIPMENTS'][operation['ID_EQUIPMENT']]['UF_NAME'] + '\n' +
                    '                </div>\n' +
                    '                <div class="col-md-3 border-bottom  border-left">\n' +
                    '                    ' + operation["DATE_START"] + ' - ' + operation["DATE_END"] + '\n' +
                    '                </div>\n' +
                    '            </div>' +
                    '\n';
            });

            contentContainer += '<hr>';
        });
    }

    contentContainer += '    </div>' +
        '<div class="col-md-12">\n' +
        '                <div id="graph-manufacturing-calendar"\n' +
        '                     style="height: '+ (50 + 42 * quantityOperation)+'px;"></div>\n' +
        '            </div>' +
        '</div><hr>';
    dataContainer.append(contentContainer);
}

function getParamsForCalculate() {
    var object = $('div#dataToCalculate');
    var idObject = object.find('input#idObject').val();
    console.log({
        ID_OBJECT: idObject,
        DATE_CALCULATE_TYPE: object.find('select#typeDate').val(),
        DATE_VALUE: object.find('input#valueDate').val(),
        MOVEMENT_TYPE: object.find('select#movementType').val(),
        BATCH_VALUE: $('div#' + object.find('select#movementType').val() + '-' + idObject).find('div#' + object.find('select#movementType').val() + '-batch').attr('data-batch'),
        MANUFACTURING_CHAIN_OBJECT: manufacturing_chain_object,
        MANUFACTURING_MAIN_CHAIN_OBJECT: manufacturing_main_chain_object

    });
    return {
        ID_OBJECT: idObject,
        DATE_CALCULATE_TYPE: object.find('select#typeDate').val(),
        DATE_VALUE: object.find('input#valueDate').val(),
        MOVEMENT_TYPE: object.find('select#movementType').val(),
        BATCH_VALUE: $('div#' + object.find('select#movementType').val() + '-' + idObject).find('div#' + object.find('select#movementType').val() + '-batch').attr('data-batch'),
        MANUFACTURING_CHAIN_OBJECT: manufacturing_chain_object,
        MANUFACTURING_MAIN_CHAIN_OBJECT: manufacturing_main_chain_object
    };
}

function callMethodByAjax(component, method, data) {
    return BX.ajax.runComponentAction(component,
        method, { // Вызывается без постфикса Action
            mode: 'class',
            data: {
                post: data
            }, // ключи объекта data соответствуют параметрам метода
        });
}