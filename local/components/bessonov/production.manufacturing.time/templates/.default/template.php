<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:18
 */

$APPLICATION->SetTitle('Планирование изготовления ' . $arResult['NAME_OBJECT']);
CJSCore::Init(array("jquery", "date"));
\Bitrix\Main\UI\Extension::load("ui.buttons");

?>
<script>console.log(<?=json_encode($arResult)?>)</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>var manufacturing_chain_object = <?=json_encode($arResult['TIME_MANUFACTURING_CHAIN_OBJECT'])?></script>
<script>var manufacturing_main_chain_object = <?=json_encode($arResult['TIME_MANUFACTURING_MAIN_CHAIN_OBJECT'])?></script>

<div class="container-fluid">
    <div class="row">
        <? if (count($arResult['TIME_MANUFACTURING_CHAIN_OBJECT']) > 1) { ?>
            <div class="col-xs-12">
                <h4>
                    Расчет цепи входящих объектов без учета выходных и режима работы
                </h4>
            </div>

            <div class="col-xs-2">
                <nav id="navbar-example3" style="position: relative;height: 350px;overflow: auto;"
                     class="navbar navbar-light bg-light flex-column">
                    <nav class="nav nav-pills flex-column">
                        <? foreach ($arResult['TIME_MANUFACTURING_CHAIN_OBJECT'] as $id => $object) { ?>
                            <a class="nav-link"
                               href="#item-<?= $id ?>"><?= $object['NAME_OBJECT'] ?></a>
                        <? } ?>
                    </nav>
                </nav>
            </div>
            <div data-spy="scroll" data-target="#navbar-example3" data-offset="0" class="col-xs-10"
                 style="position: relative;height: 350px;overflow: auto;">
                <? foreach ($arResult['TIME_MANUFACTURING_CHAIN_OBJECT'] as $id => $object) { ?>
                    <div class="row">
                        <div class="container-fluid" id="item-<?= $id ?>">
                            <div class="row" id="name-<?= $id ?>">
                                <div class="col-md-4">
                                    <h4><?= $object['NAME_OBJECT'] . ' (' . $object['QUANTITY_OBJECT'] . ' шт.)' ?></h4>
                                </div>
                            </div>
                            <div class="row" id="sequentially-<?= $id ?>">
                                <div class="col-md-4  border-bottom border-left">Последовательным</div>
                                <div class="col-md-4" id="sequentially-batch"
                                     data-batch="<?= $object['SEQUENTIALLY']['TIME']['TIME'] ?>">
                                    Партия : <?= $object['SEQUENTIALLY']['TIME']['BATCH'] ?>шт.
                                </div>
                                <div class="col-md-4 border-top">
                                    Время : <?= (int)$object['SEQUENTIALLY']['TIME']['TIME'] ?> ч.
                                    <?= (int)(60 * ($object['SEQUENTIALLY']['TIME']['TIME'] - (int)$object['SEQUENTIALLY']['TIME']['TIME'])) ?>
                                    мин.
                                </div>
                            </div>
                            <div class="row" id="parallel-<?= $id ?>">
                                <div class="col-md-4 border-bottom border-left">Параллельным</div>
                                <div class="col-md-4" id="parallel-batch"
                                     data-batch="<?= $object['PARALLEL']['TIME']['BATCH'] ?>">
                                    Оптимальная партия : <?= $object['PARALLEL']['TIME']['BATCH'] ?>шт.
                                </div>
                                <div class="col-md-4">
                                    Минимальное время : <?= (int)$object['PARALLEL']['TIME']['TIME'] ?> ч.
                                    <?= (int)(60 * ($object['PARALLEL']['TIME']['TIME'] - (int)$object['PARALLEL']['TIME']['TIME'])) ?>
                                    мин.
                                </div>
                            </div>
                            <div class="row" id="mixed-<?= $id ?>">
                                <div class="col-md-4 border-bottom border-left">Смешанным</div>
                                <div class="col-md-4 border-bottom" id="mixed-batch"
                                     data-batch="<?= $object['MIXED']['TIME']['BATCH'] ?>">
                                    Оптимальная партия : <?= $object['MIXED']['TIME']['BATCH'] ?>шт.
                                </div>
                                <div class="col-md-4 border-right border-bottom">
                                    Минимальное время : <?= (int)$object['MIXED']['TIME']['TIME'] ?> ч.
                                    <?= (int)(60 * ($object['MIXED']['TIME']['TIME'] - (int)$object['MIXED']['TIME']['TIME'])) ?>
                                    мин.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>
                                Используемое оборудование
                            </h4>
                        </div>
                        <div class="col-xs-12">
                            <div class="container-fluid">
                                <? $summaryTime = array_sum(array_column($object['EQUIPMENTS'], 'TIME')); ?>
                                <? foreach ($object['EQUIPMENTS'] as $equipment) { ?>
                                    <div class="row">
                                        <div class="col-md-3"><?= $equipment['UF_NAME'] ?></div>
                                        <div class="col-md-3"><?= $equipment['TIME'] ?> часов</div>
                                        <? $percent = number_format(100 * $equipment['TIME'] / $summaryTime, 1) ?>
                                        <div class="col-md-3"
                                             style="background-image: linear-gradient(to right, rgb(150, 239, 163) <?= $percent * 0.5 ?>%, rgba(255, 255, 255, 0) <?= $percent ?>%);">
                                            <?= $percent ?> %
                                        </div>
                                        <div class="col-md-3"><?= $equipment['UF_COUNT'] ?? 1 ?> шт.</div>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                <? } ?>
            </div>
            <div class="col-xs-12">
                <h4>
                    <hr>
                    Цикловой график
                </h4>
            </div>
            <script type="text/javascript">
                google.charts.load("current", {packages: ["timeline"]});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    var container = document.getElementById('graph-manufacturing');
                    var chart = new google.visualization.Timeline(container);
                    var dataTable = new google.visualization.DataTable();
                    dataTable.addColumn({type: 'string', id: 'Name'});
                    dataTable.addColumn({type: 'string', id: 'Duration'});
                    dataTable.addColumn({type: 'date', id: 'Start'});
                    dataTable.addColumn({type: 'date', id: 'End'});
                    dataTable.addRows([
                        <?
                        $now = mktime(0, 0, 0);
                        foreach ($arResult['TIME_MANUFACTURING_CHAIN_OBJECT'] as $key => $object) {
                        $date['START'] = $now + $object['START_TIME_FOR_GRAPH'] * 60 * 60;
                        $date['END'] = $now + $object['END_TIME_FOR_GRAPH'] * 60 * 60;
                        $date['START'] = date_parse_from_format('d.m.Y H:i', date('d.m.Y H:i', $date['START']));
                        $date['END'] = date_parse_from_format('d.m.Y H:i',  date('d.m.Y H:i', $date['END']));
                        ?>
                        [
                            '<?=$object['NAME_OBJECT']?>',
                            '<?=$object['MIN_TIME']?> ч.',
                            new Date(
                                <?=$date['START']['year']?>,
                                <?=$date['START']['month']?>,
                                <?=$date['START']['day']?>,
                                <?=$date['START']['hour']?>,
                                <?=$date['START']['minute']?>,
                                <?=$date['START']['second']?>
                            ),
                            new Date(
                                <?=$date['END']['year']?>,
                                <?=$date['END']['month']?>,
                                <?=$date['END']['day']?>,
                                <?=$date['END']['hour']?>,
                                <?=$date['END']['minute']?>,
                                <?=$date['END']['second']?>
                            )
                        ],
                        <? } ?>
                    ]);

                    var options = {
                        timeline: {colorByRowLabel: true}
                    };

                    chart.draw(dataTable, options);
                }

            </script>
            <div class="col-md-12">
                <div id="graph-manufacturing"
                     style="height: <?= 50 + 42 * count($arResult['TIME_MANUFACTURING_CHAIN_OBJECT']) ?>px;"></div>
            </div>
        <? } else { ?>
            <div class="col-xs-12">
                <h4>Общая трудоемкость изготовления объекта без учета выходных и режима работы</h4>
            </div>
            <div class="col-xs-12">
                <div class="container-fluid">
                    <div class="row" id="sequentially-<?= $arResult['ID_OBJECT']?>">
                        <div class="col-md-3">Последовательным</div>
                        <div class="col-md-3" id="sequentially-batch"
                             data-batch="<?= $arResult['SEQUENTIALLY_TIME']['BATCH'] ?>">
                            Партия : <?= $arResult['SEQUENTIALLY_TIME']['BATCH'] ?>шт.
                        </div>
                        <div class="col-md-3">
                            Время : <?= (int)$arResult['SEQUENTIALLY_TIME']['TIME'] ?> ч.
                            <?= (int)(60 * ($arResult['SEQUENTIALLY_TIME']['TIME'] - (int)$arResult['SEQUENTIALLY_TIME']['TIME'])) ?>
                            мин.
                        </div>
                    </div>
                    <div class="row" id="parallel-<?= $arResult['ID_OBJECT']?>">
                        <div class="col-md-3">Параллельным</div>
                        <div class="col-md-3" id="parallel-batch"
                             data-batch="<?= $arResult['PARALLEL_TIME']['BATCH'] ?>">
                            Оптимальная партия : <?= $arResult['PARALLEL_TIME']['BATCH'] ?>шт.
                        </div>
                        <div class="col-md-3">
                            Минимальное время : <?= (int)$arResult['PARALLEL_TIME']['TIME'] ?> ч.
                            <?= (int)(60 * ($arResult['PARALLEL_TIME']['TIME'] - (int)$arResult['PARALLEL_TIME']['TIME'])) ?>
                            мин.
                        </div>
                    </div>
                    <div class="row" id="mixed-<?= $arResult['ID_OBJECT']?>">
                        <div class="col-md-3">Смешанным</div>
                        <div class="col-md-3" id="mixed-batch" data-batch="<?= $arResult['MIXED_TIME']['BATCH'] ?>">
                            Оптимальная партия : <?= $arResult['MIXED_TIME']['BATCH'] ?>шт.
                        </div>
                        <div class="col-md-3">
                            Минимальное время : <?= (int)$arResult['MIXED_TIME']['TIME'] ?> ч.
                            <?= (int)(60 * ($arResult['MIXED_TIME']['TIME'] - (int)$arResult['MIXED_TIME']['TIME'])) ?>
                            мин.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <h4>
                    <hr>
                    Используемое оборудование
                </h4>
            </div>
            <div class="col-xs-12">
                <div class="container-fluid">
                    <? $summaryTime = array_sum(array_column($arResult['EQUIPMENTS'], 'TIME')); ?>
                    <? foreach ($arResult['EQUIPMENTS'] as $equipment) { ?>
                        <div class="row">
                            <div class="col-md-3"><?= $equipment['UF_NAME'] ?></div>
                            <div class="col-md-3"><?= $equipment['TIME'] ?> часов</div>
                            <? $percent = number_format(100 * $equipment['TIME'] / $summaryTime, 1) ?>
                            <div class="col-md-3"
                                 style="background-image: linear-gradient(to right, rgb(150, 239, 163) <?= $percent * 0.5 ?>%, rgba(255, 255, 255, 0) <?= $percent ?>%);">
                                <?= $percent ?> %
                            </div>
                            <div class="col-md-3"><?= $equipment['UF_COUNT'] ?? 1 ?> шт.</div>
                        </div>
                    <? } ?>
                </div>
            </div>

        <? } ?>


        <div class="col-xs-12">
            <h4>
                <hr>
                <? if ($arResult['IS_FINAL_OBJECT']) { ?>
                    Доступен полный расчет производственного цикла
                <? } else { ?>
                    Объект не является конечным - расчет доступен, но будет не полным
                <? } ?>
            </h4>
        </div>
        <div class="col-xs-12">
            <div class="container-fluid" id="dataToCalculate">
                <div class="row">
                    <input type="hidden" id="idObject" value="<?= $arResult['ID_OBJECT'] ?>">
                    <div class="col-md-3">Относительность расчета</div>
                    <div class="col-md-3">
                        <select id="typeDate">
                            <option value="start">От даты начала производства</option>
                            <option value="end">От даты окончания производства</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <input type="text" value="" id="valueDate"
                                   onclick="BX.calendar({node: this, field: this, bTime: true});">
                        </div>
                    </div>
                </div>
                <? if (!$arResult['TIME_MANUFACTURING_CHAIN_OBJECT']) { ?>
                <div class="row">
                    <div class="col-md-3">Вид движения изделий в производстве</div>
                    <div class="col-md-3">
                        <select id="movementType">
                            <option value="sequentially">Последовательным</option>
                            <option value="parallel">Параллельным</option>
                            <option value="mixed">Смешанным</option>
                        </select>
                    </div>
                </div>
                <? } ?>
                <div class="row">
                    <div class="col-md-3">Рабочее время</div>
                    <div class="col-md-3">
                        с <?= $arResult['WORK_TIME_COMPANY']['START'] ?> до <?= $arResult['WORK_TIME_COMPANY']['END'] ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="ui-btn-double ui-btn-primary">
                            <a class="ui-btn-main" id="planing-button" onclick="calculatePlaning()">посчитать</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container-fluid" id="result-calculate"></div>
