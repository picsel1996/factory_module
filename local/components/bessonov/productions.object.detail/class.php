<?php

/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:13
 */

use Factory\Factory,
    Factory\EquipmentTable,
    Factory\OperationTable,
    Factory\ProcessTable,
    Factory\TimeTable;

class CObjectDetail extends CBitrixComponent implements \Bitrix\Main\Engine\Contract\Controllerable
{
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    public function executeComponent()
    {
        $object = new Factory($this->arParams['PRODUCT_ID']);
        $this->arResult['NAME_OBJECT'] = $object->nameObject;
        $this->arResult['ID_OBJECT'] = $object->idObject;
        $this->arResult['ID_TECHPROCES'] = $object->techProcesObject['ID'];
        $this->arResult['OPERATIONS'] = $object->operationsObject;
        $idEquipment = array_unique(array_column($this->arResult['OPERATIONS'], 'UF_EQUIPMENT'));
        $this->arResult['EQUIPMENTS'] = $this->getEquipmentOperations($idEquipment);

        $this->includeComponentTemplate();
    }

    private function getEquipmentOperations($idEquipment = false)
    {
        $result = [];
        $filter = [];
        if ($idEquipment) {
            $filter = [
                'ID' => $idEquipment
            ];
        }
        $equipments = EquipmentTable::getList([
            'select' => ['ID', 'UF_NAME'],
            'filter' => $filter
        ])->fetchAll();
        foreach ($equipments as $equipment) {
            $result[$equipment['ID']] = $equipment;
        }

        return $result;
    }

    public function ajaxGetOperationDataAction($post)
    {
        $arResult = [];
        $arResult['EQUIPMENTS'] = $this->getEquipmentOperations();
        return $arResult;
    }

    public function ajaxGetEquipmentInfoAction($post)
    {
        return EquipmentTable::getList([
            'filter' => [
                'ID' => [$post['ID_PREVIOUS'], $post['ID_CURRENT']]
            ]
        ])->fetchAll();
    }

    public function ajaxDeleteOperationAction($post)
    {
        return OperationTable::delete($post['ID']);
    }

    public function ajaxAddNewTechProcessAction($post)
    {
        return ProcessTable::add($post);
    }

    public function ajaxSaveOperationDataAction($post)
    {
        if ($post['ID'] ?? false) {
            $idOperation = $post['ID'];
            unset($post['ID']);
            $result = OperationTable::update($idOperation, $post);
        } else {
            $result = OperationTable::add($post);
        }

        return $result;
    }

    public function ajaxSaveTransportTimeAction($post)
    {
        if (count(TimeTable::getList([
            'filter' => [
                'UF_ID_EQUIPMENTS' => $post['UF_ID_EQUIPMENTS']
            ]
        ])->fetchAll()) == 0) {
            TimeTable::add($post);
        }

        return $post;
    }

    public function configureActions()
    {
        return [];
    }

}