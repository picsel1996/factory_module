<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:18
 */

?>
<script src="https://api-maps.yandex.ru/2.1/?apikey=20738e30-92e1-48ff-9637-63a543860671&lang=ru_RU" type="text/javascript"></script>

<?
$APPLICATION->SetTitle($arResult['NAME_OBJECT']);
\Bitrix\Main\UI\Extension::load("ui.buttons");
if ($arResult ?? false) {
    $list = [];
    foreach ($arResult['OPERATIONS'] as $key => $operation) {
        $list[] = [
            'data' => [ //Данные ячеек
                "NUMBER" => ++$key,
                "UF_NAME" => $operation['UF_NAME'],
                "UF_EQUIPMENT" => $arResult['EQUIPMENTS'][$operation['UF_EQUIPMENT']]['UF_NAME'],
                "UF_TIME" => $operation['UF_TIME']
            ],
            'actions' => [ //Действия над ними
                [
                    'text' => 'Редактировать',
                    'onclick' => 'editOperation(' . $operation['ID'] . ')'
                ],
                [
                    'text' => 'Удалить',
                    'onclick' => 'deleteOperation(' . $operation['ID'] . ')'
                ]
            ],
            'id' => $operation['ID'] . '" data-number = "' . $key . '" data-name = "' . $operation['UF_NAME'] . '" data-time = "' . $operation['UF_TIME'] . '" data-equipment = "' . $arResult['EQUIPMENTS'][$operation['UF_EQUIPMENT']]['ID'],
        ];
    }
}
$this->SetViewTarget('pagetitle'); ?>
<div class="ui-btn-double ui-btn-primary tasks-interface-filter-btn-add">
    <?php if ($arResult['ID_TECHPROCES'] == null) { ?>
        <a class="ui-btn-main" id="techProcess-buttonAdd" onclick="addNewTechProces(<?= $arResult['ID_OBJECT'] ?>)">Добавить
            Техпроцесс</a>
    <?php } ?>
    <a class="ui-btn-main" id="operations-buttonAdd" onclick="addNewOperation(<?= $arResult['ID_TECHPROCES'] ?>)">Добавить
        Операцию</a>
</div>
<? if (count($arResult['OPERATIONS']) > 0) { ?>
    <div class="ui-btn-double ui-btn-primary tasks-interface-filter-btn-add">
        <a class="ui-btn-main" id="planing-button" href="planing/">Планирование</a>
    </div>
<? } ?>
<?php
$this->EndViewTarget();

$APPLICATION->IncludeComponent(
    "bitrix:main.ui.grid",
    "",
    [
        'GRID_ID' => 'grid_operations_object',
        "COLUMNS" => [
            ["id" => "NUMBER", "name" => '№', 'sort' => 'NUMBER', 'default' => true],
            ["id" => "UF_NAME", "name" => 'Операция', 'sort' => 'UF_NAME', 'default' => true],
            ["id" => "UF_EQUIPMENT", "name" => 'Оборудование', 'sort' => 'UF_EQUIPMENT', 'default' => true],
            ["id" => "UF_TIME", "name" => 'Длительность', 'sort' => 'UF_TIME', 'default' => true],
        ],
        "ROWS" => $list,
        'AJAX_MODE' => 'Y',
        'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', 'grid_operations_object'),
        'ALLOW_COLUMNS_SORT' => true,
        'ALLOW_COLUMNS_RESIZE' => true,
        'ALLOW_HORIZONTAL_SCROLL' => true,
        'ALLOW_SORT' => true,
        'ALLOW_PIN_HEADER' => true,
        'AJAX_OPTION_HISTORY' => 'N'
    ]
);
CJSCore::Init(['popup']);
?>

