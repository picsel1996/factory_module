$(function () {


});

function editOperation(idOperation) {
    var data = $('tr.main-grid-row.main-grid-row-body[data-id="' + idOperation + '"]');
    var object = {
        id: idOperation,
        name: data.attr('data-name'),
        time: data.attr('data-time'),
        equipment: data.attr('data-equipment')
    };
    callMethodByAjax(
        'bessonov:productions.object.detail',
        'ajaxGetOperationData',
        {check: true}).then(
        function (result) {
            var equipments = result.data['EQUIPMENTS'];
            var content = '<div class="container-fluid">\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Операция</div>\n' +
                '        <div class="col-xs-6">' +
                '            <input id="operation_' + object.id + '" type="text" placeholder="Операция" value="' + object.name + '" class="form-control input-md">' +
                '        </div>\n' +
                '    </div>\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Оборудование</div>\n' +
                '        <div class="col-xs-6">' +
                '            <select id="equipment_' + object.id + '" name="selectbasic" class="form-control">\n';
            $.each(equipments, function (id, value) {
                if (equipments[object.equipment]['ID'] == id) {
                    content += '<option selected value="' + value['ID'] + '">' + value['UF_NAME'] + '</option>\n';
                } else {
                    content += '<option value="' + value['ID'] + '">' + value['UF_NAME'] + '</option>\n';
                }
            });
            content += '</select>' +
                '        </div>\n' +
                '    </div>\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Длительность</div>\n' +
                '        <div class="col-xs-6">' +
                '            <input id="time_' + object.id + '" type="number" placeholder="Длительность" value="' + object.time + '" class="form-control input-md">' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>';
            editElement(object, content)
        });

}

function addNewOperation(idTechProces) {
    callMethodByAjax(
        'bessonov:productions.object.detail',
        'ajaxGetOperationData',
        {check: true}).then(
        function (result) {
            var equipments = result.data['EQUIPMENTS'];
            var content = '<div class="container-fluid">\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Операция</div>\n' +
                '        <div class="col-xs-6">' +
                '            <input id="operation" type="text" placeholder="Операция" value="" class="form-control input-md">' +
                '        </div>\n' +
                '    </div>\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Оборудование</div>\n' +
                '        <div class="col-xs-6">' +
                '            <select id="equipment" name="selectbasic" class="form-control">\n';
            $.each(equipments, function (id, value) {
                content += '<option value="' + value['ID'] + '">' + value['UF_NAME'] + '</option>\n';
            });
            content += '</select>' +
                '        </div>\n' +
                '    </div>\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Длительность</div>\n' +
                '        <div class="col-xs-6">' +
                '            <input id="time" type="number" placeholder="Длительность" value="" class="form-control input-md">' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>';
            addElement(content, idTechProces);
        });
}

function addNewTechProces(idObject) {
    var content = '<div class="container-fluid">\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Название техпроцесса</div>\n' +
        '        <div class="col-xs-6">' +
        '            <input id="name" type="text" placeholder="Название техпроцесса" value="" class="form-control input-md">' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
    addTechproces(content, idObject);
}

function deleteOperation(idOperation) {
    callMethodByAjax(
        'bessonov:productions.object.detail',
        'ajaxDeleteOperation',
        {ID: idOperation}
    ).then(function () {
        reloadGrid('grid_operations_object');
    });
}

//формы редактирования
function editElement(object, content) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-edition_" + object.id, BX('element'), {
        content: content,
        width: 600, // ширина окна
        height: 300, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Редактирование :' + object.name,
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-' + object.id, // идентификатор
                idOperation: object.id,
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-edition_' + object.id).find('div.container-fluid');
                        var data = {
                            ID: object.id,
                            UF_NAME: formData.find('input#operation_' + object.id).val(),
                            UF_EQUIPMENT: formData.find('select#equipment_' + object.id).val(),
                            UF_TIME: formData.find('input#time_' + object.id).val()
                        };
                        callMethodByAjax(
                            'bessonov:productions.object.detail',
                            'ajaxSaveOperationData',
                            data
                        ).then(function () {
                            popup.close();
                            reloadGrid('grid_operations_object');
                            var currentNumber = Number($('tr[data-id="' + object.id + '"]').attr('data-number'));
                            if (currentNumber > 1) {
                                ajaxAddTransportationTime(
                                    $('tr[data-number="' + (currentNumber - 1) + '"]').attr('data-equipment'),
                                    $('tr[data-number="' + currentNumber + '"]').attr('data-equipment')
                                );
                            }
                            if ($('tr[data-number="' + (currentNumber + 1) + '"]').length > 0) {
                                ajaxAddTransportationTime(
                                    $('tr[data-number="' + currentNumber + '"]').attr('data-equipment'),
                                    $('tr[data-number="' + (currentNumber + 1) + '"]').attr('data-equipment')
                                );
                            }
                        });
                    }
                }
            })
        ],
        events: {
            onPopupShow: function () {
                // Событие при показе окна
            },
            onPopupClose: function () {
                // Событие при закрытии окна
            }
        }
    });
    popup.show();
}

//добавление техпроцесса
function addTechproces(content, idObject) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-add-techproces", BX('element'), {
        content: content,
        width: 600, // ширина окна
        height: 300, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Создание техпроцеса объекта',
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-add-techprocess', // идентификатор
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-add-techproces').find('div.container-fluid');
                        var data = {
                            UF_NAME: formData.find('input#name').val(),
                            UF_PRODUCTION: idObject,
                        };
                        callMethodByAjax(
                            'bessonov:productions.object.detail',
                            'ajaxAddNewTechProcess',
                            data
                        ).then(function () {
                            popup.close();
                            location.reload();
                        });
                    }
                }
            })
        ]
    });
    popup.show();
}

//формы добавления
function addElement(content, idTechProces) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-add-operation", BX('element'), {
        content: content,
        width: 600, // ширина окна
        height: 300, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Создание новой операции',
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-add-operation', // идентификатор
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-add-operation').find('div.container-fluid');
                        var data = {
                            UF_TECH_PROC: idTechProces,
                            UF_NAME: formData.find('input#operation').val(),
                            UF_EQUIPMENT: formData.find('select#equipment').val(),
                            UF_TIME: formData.find('input#time').val()
                        };
                        callMethodByAjax(
                            'bessonov:productions.object.detail',
                            'ajaxSaveOperationData',
                            data
                        ).then(function () {
                            popup.close();

                            ajaxAddTransportationTime($('tr[data-equipment]:last').attr('data-equipment'), formData.find('select#equipment').val());
                            reloadGrid('grid_operations_object');
                        });
                    }
                }
            })
        ],
        events: {
            onPopupShow: function () {
                // Событие при показе окна
            },
            onPopupClose: function () {
                // Событие при закрытии окна
            }
        }
    });
    popup.show();
}

function ajaxAddTransportationTime(idPreviousEquipment, idCurrentEquipment) {
    console.log(idPreviousEquipment, idCurrentEquipment);
    callMethodByAjax(
        'bessonov:productions.object.detail',
        'ajaxGetEquipmentInfo',
        {
            ID_PREVIOUS: idPreviousEquipment,
            ID_CURRENT: idCurrentEquipment
        }
    ).then(function (result) {
        if (result.data.length > 1) {
            ymaps.route([result.data[0]['UF_COORDINATE'], result.data[1]['UF_COORDINATE']], {
                mapStateAutoApply: true,
                avoidTrafficJams: true,
                multiRoute: false,
                routingMode: "auto",
                viaIndexes: []
            }).then(function (route) {
                console.log(route.properties._data.RouterRouteMetaData.time);
                if (route.properties._data.RouterRouteMetaData.time > 0) {
                    callMethodByAjax(
                        'bessonov:productions.object.detail',
                        'ajaxSaveTransportTime',
                        {
                            UF_ID_EQUIPMENTS: result.data[0]['ID'] + '.' + result.data[1]['ID'],
                            UF_TIME: route.properties._data.RouterRouteMetaData.time / (60 * 60)
                        }
                    );
                }
            }, function (error) {
                console.log(error);
            });
        }
    });
}

//отправка аякс
function callMethodByAjax(component, method, data) {
    return BX.ajax.runComponentAction(component,
        method, { // Вызывается без постфикса Action
            mode: 'class',
            data: {
                post: data
            }, // ключи объекта data соответствуют параметрам метода
        });
}

function reloadGrid(idGrid) {
    var reloadParams = {apply_filter: 'Y', clear_nav: 'Y'};
    var gridObject = BX.Main.gridManager.getById(idGrid);
    gridObject.instance.reloadTable('POST', reloadParams);
}