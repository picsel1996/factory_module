<?php

/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:13
 */

use Factory\WorkshopTable,
    Factory\StockTable,
    Factory\BranchTable,
    Factory\Factory;

class CStructureList extends CBitrixComponent implements \Bitrix\Main\Engine\Contract\Controllerable
{
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    public function executeComponent()
    {
        $object = new Factory(false, 'structural_object');

        $this->arResult['STRUCTURE_TREE'] = $this->getTreeStructure($object);
        $this->includeComponentTemplate();
    }

    private function getTreeStructure($object)
    {
        $tree = array_combine(array_column($object->branch, 'ID'), $object->branch);

        foreach ($object->workshops as $workshop) {
            $tree[$workshop['UF_BRANCH']]['WORKSHOPS'][$workshop['ID']] = $workshop;
        }
        return $tree;
    }

    public function ajaxGetBranchesDataAction($post)
    {
        return BranchTable::getList()->fetchAll();
    }

    public function ajaxSaveNewBranchDataAction($post)
    {
        return BranchTable::add($post);
    }

    public function ajaxSaveNewWorkshopDataAction($post)
    {
        return WorkshopTable::add($post);
    }

    public function configureActions()
    {
        return [];
    }

}