<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:18
 */

?><script>console.log(<?=json_encode($arResult)?>)</script><?
\Bitrix\Main\UI\Extension::load("ui.buttons");

foreach ($arResult['STRUCTURE_TREE'] as $branch) { ?>
    <ul>
        <li><?= $branch['UF_NAME'] ?></li>
        <ul>
            <? foreach ($branch['WORKSHOPS'] as $workshop) { ?>
                <li><a href="/factory/production_structure/<?=$workshop['ID']?>/"><?= $workshop['UF_NAME'] ?></a></li>
            <? } ?>
        </ul>
    </ul>
<? }

$this->SetViewTarget('pagetitle'); ?>
    <div class="ui-btn-double ui-btn-primary tasks-interface-filter-btn-add">
        <a class="ui-btn-main" id="branch-buttonAdd" onclick="addNewBranch(<?= $arResult['WORKSHOP']['ID'] ?>)">Добавить
            Филиал</a>
    </div>
<? if (count($arResult['STRUCTURE_TREE']) > 0) ?>
    <div class="ui-btn-double ui-btn-primary tasks-interface-filter-btn-add">
        <a class="ui-btn-main" id="workshop-buttonAdd" onclick="addNewWorkshop(<?= $arResult['WORKSHOP']['ID'] ?>)">Добавить
            Цех</a>
    </div>
<!--    <div class="ui-btn-double ui-btn-primary tasks-interface-filter-btn-add">-->
<!--        <a class="ui-btn-main" id="stock-buttonAdd" onclick="#">Добавить-->
<!--            Склад</a>-->
<!--    </div>-->
<?php
$this->EndViewTarget();

