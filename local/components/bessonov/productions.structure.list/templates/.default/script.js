function addNewWorkshop() {
    callMethodByAjax(
        'bessonov:productions.structure.list',
        'ajaxGetBranchesData',
        {send: true}
    ).then(
        function (result) {
            console.log(result);
            var branch = result.data;
            var content = '<div class="container-fluid">\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Название</div>\n' +
                '        <div class="col-xs-6">' +
                '            <input id="name" type="text" placeholder="Название" value="" class="form-control input-md">' +
                '        </div>\n' +
                '    </div>\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Филиал</div>\n' +
                '        <div class="col-xs-6">' +
                '            <select id="branch" name="selectbasic" class="form-control">\n';
            $.each(branch, function (id, value) {
                content += '<option value="' + value['ID'] + '">' + value['UF_NAME'] + '</option>\n';
            });
            content += '</select>' +
                '        </div>\n' +
                '    </div>\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Местоположение</div>\n' +
                '        <div class="col-xs-6">' +
                '            <input id="coordinate" type="text" placeholder="Местоположение" value="" class="form-control input-md">' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>';
            addWorkshop(content)
        },
        function (result) {
            alert('Возникли проблемы! Повторите позже');
        }
    );

}

function addNewBranch() {
    var content = '<div class="container-fluid">\n' +
        '    <div class="row">\n' +
        '        <div class="col-xs-6">Название</div>\n' +
        '        <div class="col-xs-6">' +
        '            <input id="name" type="text" placeholder="Название" value="" class="form-control input-md">' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
    addBranch(content);
}

function deleteEquipments(idEquipment, idWorkshop) {
    callMethodByAjax(
        'bessonov:productions.structure.detail',
        'ajaxDeleteEquipment',
        {
            ID_EQUIPMENT: idEquipment,
            ID_WORKSHOP: idWorkshop
        }
    ).then(function () {
        reloadGrid('grid_equipments_object');
    });
}

//формы редактирования
function editElement(object, content) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-edition_" + object.id, BX('element'), {
        content: content,
        width: 600, // ширина окна
        height: 300, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Редактирование :' + object.name,
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-' + object.id, // идентификатор
                idOperation: object.id,
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-edition_' + object.id).find('div.container-fluid');
                        var data = {
                            ID: object.id,
                            UF_NAME: formData.find('input#name').val(),
                            UF_DESCRIPTION: formData.find('input#description').val(),
                            UF_COUNT: parseInt(formData.find('input#quantity').val()),
                            UF_COORDINATE: formData.find('input#coordinate').val(),
                        };
                        callMethodByAjax(
                            'bessonov:productions.structure.detail',
                            'ajaxSaveEquipmentData',
                            data
                        ).then(function () {
                            popup.close();
                            reloadGrid('grid_equipments_object');
                        });
                    }
                }
            })
        ],
        events: {
            onPopupShow: function () {
                // Событие при показе окна
            },
            onPopupClose: function () {
                // Событие при закрытии окна
            }
        }
    });
    popup.show();
}

//формы добавления
function addBranch(content) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-add-branch", BX('element'), {
        content: content,
        width: 600, // ширина окна
        height: 300, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Добавление нового филиала',
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-add-branch', // идентификатор
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-add-branch').find('div.container-fluid');
                        var data = {
                            UF_NAME: formData.find('input#name').val(),
                        };
                        callMethodByAjax(
                            'bessonov:productions.structure.list',
                            'ajaxSaveNewBranchData',
                            data
                        ).then(
                            function (result) {
                                popup.close();
                                location.reload();
                            },
                            function (result) {
                                popup.close();
                                alert('Возникли проблемы! Повторите позже');
                            }
                        );
                    }
                }
            })
        ],
        events: {
            onPopupShow: function () {
                // Событие при показе окна
            },
            onPopupClose: function () {
                // Событие при закрытии окна
            }
        }
    });
    popup.show();
}

function addWorkshop(content) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-add-workshop", BX('element'), {
        content: content,
        width: 600, // ширина окна
        height: 300, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Добавление нового цеха',
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-add-workshop', // идентификатор
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-add-workshop').find('div.container-fluid');
                        var data = {
                            UF_NAME: formData.find('input#name').val(),
                            UF_BRANCH: formData.find('select#branch').val(),
                            UF_COORDINATE: formData.find('input#coordinate').val(),
                        };
                        callMethodByAjax(
                            'bessonov:productions.structure.list',
                            'ajaxSaveNewWorkshopData',
                            data
                        ).then(
                            function (result) {
                                popup.close();
                                location.reload();
                            },
                            function (result) {
                                popup.close();
                                alert('Возникли проблемы! Повторите позже');
                            }
                        );
                    }
                }
            })
        ],
        events: {
            onPopupShow: function () {
                // Событие при показе окна
            },
            onPopupClose: function () {
                // Событие при закрытии окна
            }
        }
    });
    popup.show();
}

//отправка аякс
function callMethodByAjax(component, method, data) {
    return BX.ajax.runComponentAction(component,
        method, { // Вызывается без постфикса Action
            mode: 'class',
            data: {
                post: data
            }, // ключи объекта data соответствуют параметрам метода
        });
}

function reloadGrid(idGrid) {
    var reloadParams = {apply_filter: 'Y', clear_nav: 'Y'};
    var gridObject = BX.Main.gridManager.getById(idGrid);
    gridObject.instance.reloadTable('POST', reloadParams);
}