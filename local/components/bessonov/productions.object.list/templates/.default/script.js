function addNewObject() {
    callMethodByAjax(
        'bessonov:productions.object.list',
        'ajaxGetProductions',
        {send: true}
    ).then(
        function (result) {
            console.log(result);
            var object = result.data;
            var content = '<div class="container-fluid">\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Название</div>\n' +
                '        <div class="col-xs-6">' +
                '            <input id="name" type="text" placeholder="Название" value="" class="form-control input-md">' +
                '        </div>\n' +
                '    </div>\n' +
                '    <div class="row">\n' +
                '        <div class="col-xs-6">Родительский объект</div>\n' +
                '        <div class="col-xs-6">' +
                '            <select id="parent" name="selectbasic" class="form-control">\n'+
                '               <option value=""></option>\n';
            $.each(object, function (id, value) {
                content += '<option value="' + value['ID'] + '">' + value['UF_NAME'] + '</option>\n';
            });
            content += '</select>' +
                '        </div>\n' +
                '    </div>\n' +
                    '    <div class="row">\n' +
                '        <div class="col-xs-6">Количество</div>\n' +
                '        <div class="col-xs-6">' +
                '            <input id="count" type="number" placeholder="Количество" value="" class="form-control input-md">' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>';
            addWorkshop(content)
        },
        function (result) {
            console.log(result);
            alert('Возникли проблемы! Повторите позже');

        }
    );

}

function addWorkshop(content) {
    BX.element = null;
    var popup = BX.PopupWindowManager.create("popup-add-object", BX('element'), {
        content: content,
        width: 600, // ширина окна
        height: 300, // высота окна
        zIndex: 100, // z-index
        closeIcon: {
            // объект со стилями для иконки закрытия, при null - иконки не будет
            opacity: 1
        },
        titleBar: 'Добавление нового объекта',
        closeByEsc: true, // закрытие окна по esc
        darkMode: false, // окно будет светлым или темным
        autoHide: false, // закрытие при клике вне окна
        draggable: true, // можно двигать или нет
        resizable: true, // можно ресайзить
        min_height: 100, // минимальная высота окна
        min_width: 100, // минимальная ширина окна
        lightShadow: true, // использовать светлую тень у окна
        angle: false, // появится уголок
        overlay: {
            // объект со стилями фона
            backgroundColor: 'black',
            opacity: 500
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить', // текст кнопки
                id: 'save-btn-add-workshop', // идентификатор
                className: 'ui-btn ui-btn-success', // доп. классы
                events: {
                    click: function () {
                        // Событие при клике на кнопку
                        var formData = $('div#popup-add-object').find('div.container-fluid');
                        var data = {
                            UF_NAME: formData.find('input#name').val(),
                            UF_PARENT_ID_PRODUCT: formData.find('select#parent').val(),
                            UF_COUNT: formData.find('input#count').val(),
                        };
                        callMethodByAjax(
                            'bessonov:productions.object.list',
                            'ajaxSaveNewWorkshopData',
                            data
                        ).then(
                            function (result) {
                                popup.close();
                                location.reload();
                            },
                            function (result) {
                                popup.close();
                                alert('Возникли проблемы! Повторите позже');
                            }
                        );
                    }
                }
            })
        ],
        events: {
            onPopupShow: function () {
                // Событие при показе окна
            },
            onPopupClose: function () {
                // Событие при закрытии окна
            }
        }
    });
    popup.show();
}

//отправка аякс
function callMethodByAjax(component, method, data) {
    return BX.ajax.runComponentAction(component,
        method, { // Вызывается без постфикса Action
            mode: 'class',
            data: {
                post: data
            }, // ключи объекта data соответствуют параметрам метода
        });
}

function reloadGrid(idGrid) {
    var reloadParams = {apply_filter: 'Y', clear_nav: 'Y'};
    var gridObject = BX.Main.gridManager.getById(idGrid);
    gridObject.instance.reloadTable('POST', reloadParams);
}