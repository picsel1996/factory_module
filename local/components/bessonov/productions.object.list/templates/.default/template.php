<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:18
 */

use Factory\Factory;
\Bitrix\Main\UI\Extension::load("ui.buttons");

echo $arResult['TREE_PRODUCTIONS'];

$this->SetViewTarget('pagetitle'); ?>
    <div class="ui-btn-double ui-btn-primary tasks-interface-filter-btn-add">
        <a class="ui-btn-main" id="objaect-buttonAdd" onclick="addNewObject()">Добавить объект производства</a>
    </div>
<?php
$this->EndViewTarget();