<?php

/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 17.03.2019
 * Time: 18:13
 */

use Factory\ProductionTable,
    Factory\Factory;

class CObjectList extends CBitrixComponent implements \Bitrix\Main\Engine\Contract\Controllerable
{
    public function executeComponent()
    {
        $arProductions = ProductionTable::getList([
            'select' => ['*'],
            'order' => ['UF_PARENT_ID_PRODUCT' => 'ASC']
        ])->fetchAll();

        $this->arResult['TREE_PRODUCTIONS'] = $this->getTreeProductions($arProductions);

        $this->arResult['TREE_PRODUCTIONS'] = $this->drawTreeProductions($this->arResult['TREE_PRODUCTIONS']);
        $this->includeComponentTemplate();
    }

    private function getTreeProductions($arProductions, $tree = [])
    {
        foreach ($arProductions as $production) {
            if ($production['UF_PARENT_ID_PRODUCT'] == null) {
                $tree[$production['ID']] = Factory::getTreeObject($production['ID'])[$production['ID']];
            }
        }

        return $tree;
    }

    private function drawTreeProductions($tree)
    {

        ob_start();
        ?>
        <ul>
            <? foreach ($tree as $id => $item) {
                if (is_int($id)) {
                    ?>
                    <li id="<?= $id ?>">
                        <a href="/factory/productions_object/<?= $id ?>/"><?= $item['NAME'] ?></a>
                    </li>
                    <? if (is_array($item)) {
                        $arElem = [];
                        foreach ($item as $key => $elem) {
                            if (is_int($key)) {
                                $arElem[$key] = $elem;
                            }
                        }
                        if (is_array($arElem)) echo $this->drawTreeProductions($arElem);
                    }
                }
            } ?>
        </ul>
        <?
        $treeTemplate = ob_get_contents();
        ob_end_clean();
        return $treeTemplate;
    }

    public function ajaxGetProductionsAction($post)
    {
        return  ProductionTable::getList([
            'select' => ['*'],
            'order' => ['UF_PARENT_ID_PRODUCT' => 'ASC']
        ])->fetchAll();
    }

    public function ajaxSaveNewWorkshopDataAction($post)
    {
        return ProductionTable::add($post);
    }

    public function configureActions()
    {
        return [];
    }
}