<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 30.03.2019
 * Time: 20:56
 */
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

class Productions extends CBitrixComponent implements \Bitrix\Main\Engine\Contract\Controllerable
{

    private $templatePage;

    public function onPrepareComponentParams($arParams)
    {
        $result = [];

        $result['IBLOCK_CODE'] = trim($arParams['IBLOCK_CODE']);
        $result['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);

        $result['DEFAULT_URL_TEMPLATES_404'] = [
            'production_object_detail' => 'productions_object/#production_object_id#/',
            'production_object_list' => 'productions_object/',
            'production_structure_detail' => 'production_structure/#production_structure_id#/',
            'production_structure_list' => 'production_structure/',
            'production_object_planing' => 'productions_object/#production_object_id#/planing/'
        ];

        $result['COMPONENT_VARIABLES'] = ['production_object_id', 'production_structure_id'];
        $result['SEF_MODE'] = 'Y';
        $result['SEF_FOLDER'] = $arParams['SEF_FOLDER'] ? $arParams['SEF_FOLDER'] : '/factory/';
        $result['SEF_URL_TEMPLATES'] = $arParams['SEF_URL_TEMPLATES'] ? $arParams['SEF_URL_TEMPLATES'] : [];
        $result['VARIABLE_ALIASES'] = $arParams['VARIABLE_ALIASES'] ? $arParams['VARIABLE_ALIASES'] : [];

        return $result;
    }

    public function executeComponent()
    {
        $this->setPage();
        $this->setResult();

        $this->includeComponentTemplate($this->templatePage);
    }

    private function getOperationsProduct()
    {

    }

    private function setPage()
    {
        $urlTemplates = [];
        $variables = [];

        if ($this->arParams['SEF_MODE'] === 'Y') {
            $urlTemplates = \CComponentEngine::MakeComponentUrlTemplates(
                $this->arParams['DEFAULT_URL_TEMPLATES_404'],
                $this->arParams['SEF_URL_TEMPLATES']
            );

            $variableAliases = \CComponentEngine::MakeComponentVariableAliases(
                $this->arParams['COMPONENT_VARIABLES'],
                $this->arParams['VARIABLE_ALIASES']
            );

            $this->templatePage = \CComponentEngine::ParseComponentPath(
                $this->arParams['SEF_FOLDER'],
                $urlTemplates,
                $variables
            );

            \CComponentEngine::InitComponentVariables(
                $this->templatePage,
                $this->componentVariables,
                $variableAliases,
                $variables
            );
        } else {
            $this->templatePage = $this->defaultPage;
        }

        $this->sefFolder = $this->arParams['SEF_FOLDER'];
        $this->urlTemplates = $urlTemplates;
        $this->variables = $variables;
        $this->variableAliases = $variableAliases;
    }

    private function setResult()
    {
        $this->arResult = [
            'FOLDER' => $this->arParams['SEF_FOLDER'],
            'URL_TEMPLATES' => array_merge($this->urlTemplates, $this->arParams['DEFAULT_URL_TEMPLATES_404']),
            'VARIABLES' => $this->variables,
        ];

        $this->arParams['SEF_URL_TEMPLATES'] = $this->arResult['URL_TEMPLATES'];
    }

    private function ajaxAction($post)
    {
        return $post;
    }

    public function configureActions()
    {
        return [];
    }
}