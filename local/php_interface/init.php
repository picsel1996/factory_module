<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('factory.d7')) {
    echo 'Module "factory.d7" not installed';
    //exit;
}
